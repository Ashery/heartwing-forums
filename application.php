<?php
/**
* @package application.php
* @copyright (c) JimA http://beta-garden.com 2009
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

$application_form = 1;

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/application');

// You need to login before being able to send out an application
if ($user->data['user_id'] == ANONYMOUS)
{
    login_box('', $user->lang['LOGIN_APPLICATION_FORM']);
}

include($phpbb_root_path . 'includes/functions_posting.' . $phpEx);

// Let's set the configuration, this is the ID of the forum where the post goes to
$forumid_send = 2;
$application_type = 
$submit = (isset($_POST['submit'])) ? true : false;

	if ($submit)
	{

		if ($application_form == 1) {
		// Setting the variables we need to submit the post to the forum where all the applications come in
		$apply_subject  = '['.utf8_normalize_nfc(request_var('adulthood', '', true)).'] '.' '.utf8_normalize_nfc(request_var('birthname', '', true));
		$apply_post     = 
		sprintf($user->lang['APPLICATION_MESSAGE'], 
		utf8_normalize_nfc(request_var('image', '', true)),
		utf8_normalize_nfc(request_var('birthname', '', true)),  
		utf8_normalize_nfc(request_var('dragonflight', '', true)), 
		utf8_normalize_nfc(request_var('dfcolor', '', true)),
		utf8_normalize_nfc(request_var('alias', '', true)), 
		utf8_normalize_nfc(request_var('age', '', true)), 
		utf8_normalize_nfc(request_var('gender', '', true)),
		utf8_normalize_nfc(request_var('adulthood', '', true)),
		utf8_normalize_nfc(request_var('loyalties', '', true)),
		utf8_normalize_nfc(request_var('view', '', true)),
		utf8_normalize_nfc(request_var('affiliations', '', true)),
		utf8_normalize_nfc(request_var('attitude', '', true)),
		utf8_normalize_nfc(request_var('traits', '', true)),
		utf8_normalize_nfc(request_var('authority', '', true)),
		utf8_normalize_nfc(request_var('characterpersonality', '', true)),
		utf8_normalize_nfc(request_var('irlpersonality', '', true)),
		utf8_normalize_nfc(request_var('hobbies', '', true)),
		utf8_normalize_nfc(request_var('occupation', '', true)),
		utf8_normalize_nfc(request_var('position', '', true)),
		utf8_normalize_nfc(request_var('common', '', true)),
		utf8_normalize_nfc(request_var('prestige', '', true)),
		utf8_normalize_nfc(request_var('backstory', '', true)),
		utf8_normalize_nfc(request_var('experience', '', true)),
		utf8_normalize_nfc(request_var('ic-reason', '', true)),
		utf8_normalize_nfc(request_var('ooc-reason', '', true)),
		utf8_normalize_nfc(request_var('understand', '', true)),
		utf8_normalize_nfc(request_var('timeline', '', true)),
		utf8_normalize_nfc(request_var('lore', '', true))
		); 
		// variables to hold the parameters for submit_post
		$poll = $uid = $bitfield = $options = ''; 

		generate_text_for_storage($apply_post, $uid, $bitfield, $options, true, true, true);

		$data = array( 
			'forum_id'      => 8,
			'icon_id'		=> 0,

			'enable_bbcode'		=> true,
			'enable_smilies'	=> true,
			'enable_urls'		=> true,
			'enable_sig'		=> true,

			'message'		=> $apply_post,
			'message_md5'	=> md5($apply_post),
						
			'bbcode_bitfield'	=> $bitfield,
			'bbcode_uid'		=> $uid,

			'post_edit_locked'	=> 0,
			'topic_title'		=> $apply_subject,
			'notify_set'		=> false,
			'notify'			=> false,
			'post_time' 		=> 0,
			'forum_name'		=> '',
			'enable_indexing'	=> true,
		);

		// Sending the post to the forum set in configuration above
		submit_post('post', $apply_subject, '', POST_NORMAL, $poll, $data);

		$message = $user->lang['APPLICATION_SEND'];
		$message = $message . '<br /><br />' . sprintf($user->lang['RETURN_INDEX'], '<a href="' . append_sid("{$phpbb_root_path}index.$phpEx") . '">', '</a>');
		trigger_error($message);
		}
		else if ($application_form == 2) {
		// Setting the variables we need to submit the post to the forum where all the applications come in
		$apply_subject  = '[DRAGON_2]'.' '.utf8_normalize_nfc(request_var('birthname', '', true));
		$apply_post     = 
		sprintf($user->lang['APPLICATION_MESSAGE'], 
		utf8_normalize_nfc(request_var('image', '', true)),
		utf8_normalize_nfc(request_var('birthname', '', true)),  
		utf8_normalize_nfc(request_var('dragonflight', '', true)), 
		utf8_normalize_nfc(request_var('dfcolor', '', true)),
		utf8_normalize_nfc(request_var('alias', '', true)), 
		utf8_normalize_nfc(request_var('age', '', true)), 
		utf8_normalize_nfc(request_var('gender', '', true)),
		utf8_normalize_nfc(request_var('adulthood', '', true)),
		utf8_normalize_nfc(request_var('loyalties', '', true)),
		utf8_normalize_nfc(request_var('view', '', true)),
		utf8_normalize_nfc(request_var('affiliations', '', true)),
		utf8_normalize_nfc(request_var('attitude', '', true)),
		utf8_normalize_nfc(request_var('traits', '', true)),
		utf8_normalize_nfc(request_var('authority', '', true)),
		utf8_normalize_nfc(request_var('characterpersonality', '', true)),
		utf8_normalize_nfc(request_var('irlpersonality', '', true)),
		utf8_normalize_nfc(request_var('hobbies', '', true)),
		utf8_normalize_nfc(request_var('occupation', '', true)),
		utf8_normalize_nfc(request_var('position', '', true)),
		utf8_normalize_nfc(request_var('common', '', true)),
		utf8_normalize_nfc(request_var('prestige', '', true)),
		utf8_normalize_nfc(request_var('backstory', '', true)),
		utf8_normalize_nfc(request_var('experience', '', true)),
		utf8_normalize_nfc(request_var('ic-reason', '', true)),
		utf8_normalize_nfc(request_var('ooc-reason', '', true)),
		utf8_normalize_nfc(request_var('understand', '', true)),
		utf8_normalize_nfc(request_var('timeline', '', true)),
		utf8_normalize_nfc(request_var('lore', '', true))
		); 
		// variables to hold the parameters for submit_post
		$poll = $uid = $bitfield = $options = ''; 

		generate_text_for_storage($apply_post, $uid, $bitfield, $options, true, true, true);

		$data = array( 
			'forum_id'      => 8,
			'icon_id'		=> 0,

			'enable_bbcode'		=> true,
			'enable_smilies'	=> true,
			'enable_urls'		=> true,
			'enable_sig'		=> true,

			'message'		=> $apply_post,
			'message_md5'	=> md5($apply_post),
						
			'bbcode_bitfield'	=> $bitfield,
			'bbcode_uid'		=> $uid,

			'post_edit_locked'	=> 0,
			'topic_title'		=> $apply_subject,
			'notify_set'		=> false,
			'notify'			=> false,
			'post_time' 		=> 0,
			'forum_name'		=> '',
			'enable_indexing'	=> true,
		);

		// Sending the post to the forum set in configuration above
		submit_post('post', $apply_subject, '', POST_NORMAL, $poll, $data);

		$message = $user->lang['APPLICATION_SEND'];
		$message = $message . '<br /><br />' . sprintf($user->lang['RETURN_INDEX'], '<a href="' . append_sid("{$phpbb_root_path}index.$phpEx") . '">', '</a>');
		trigger_error($message);
		}
}

page_header($user->lang['APPLICATION_PAGETITLE']);





$template->assign_vars(array(
	'PROCESS_APPFORM'	=> append_sid("{$phpbb_root_path}application.$phpEx"),
	));
	
$template->set_filenames(array(
    'body' => 'appform_body.php',
));

page_footer();


?>

