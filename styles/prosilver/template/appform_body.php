<!-- INCLUDE overall_header.html -->
<script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>
<script>




jQuery (document).ready (function() {   
    jQuery('#df').unbind().on('change',function(){
        var system = jQuery(this).find('option:selected').val();
        var dfcolor = jQuery("#dfcolor").val();
        var notice = jQuery("age-notice"); 
        var age = jQuery("age").val();

        if (system == "Red Dragonflight") { 
            color = "#B22222"; 
            console.log(jQuery("#dfcolor").val); 
            jQuery("#dfcolor").val(color);
            jQuery("#age-notice").css("display", "block");
        }
        else if (system == "Blue Dragonflight") {
            color = "#00BFFF"; 
            console.log(jQuery("#dfcolor").val); 
            jQuery("#dfcolor").val(color);
            jQuery("#age-notice").css("display", "block");
        }
        else if (system == "Green Dragonflight") {
            color = "#90EE90"; 
            console.log(jQuery("#dfcolor").val); 
            jQuery("#dfcolor").val(color);
            jQuery("#age-notice").css("display", "block");   
        }
        else if (system == "Bronze Dragonflight") {
            color = "#DAA520"; 
            console.log(jQuery("#dfcolor").val); 
            jQuery("#dfcolor").val(color);   
            jQuery("#age-notice").css("display", "block");
        }
        else if (system == "Netherwing") {
            color = "#00CED1"; 
            console.log(jQuery("#dfcolor").val); 
            jQuery("#dfcolor").val(color);    
            jQuery("#age-notice").css("display", "block");
        }



    });
});

</script>
<div class="panel application-form">
   <div class="inner"><span class="corners-top"><span></span></span>

   <div class="content">
      
        <h2>Dragon Application</h2>
        <div class="change-template">
            What do you want to apply as?<br />

            Disclaimer: You can still apply as drakonid by going to the forums <a href="http://heartwing.dk/viewforum.php?f=8">here. </a>and follow the instructions there.
            <br />
            <br />
            <br />
            <ul>
                <li><a href="/application.php"><a href="/application.php"><img src="/images/application_icons/Dragon.png" alt="Dragon Application" /></a></li>
                <li class="disabled"><a href="/application_mortal.php"><img src="/images/application_icons/Mortal.png" alt="Mortal Application" /></a></li>
                <li class="disabled"><a href="/application_dragonspawn.php"><img src="/images/application_icons/Dragonspawn.png" alt="Dragonspawn Application" /></a></li>
                <li class="disabled perma-disabled"><a href="#"><img src="/images/application_icons/Drakonid.png" alt="Drakonid Application" /><span>COMING SOON</span></a></li>
            </ul>

        </div>

        <span style="font-size: 1.7em; color: #b9ff9a; ">
            <div class="info rookiediscord">
                Don't want your application to be posted in public? Got questions about backstory, character specialties? Questionable story? 
                Read our alternative recruitment post here: <a href="http://heartwing.dk/viewtopic.php?f=8&t=659">Alternative Recruitment - Rookie Camp</a>
            </div>
        </span>
        <br>
        <br>

        <span style="font-size: 1.3em;">
            <div class="info">
            <strong>Why do we use applications?</strong><br>
            Various writers have a different viewpoints ranging from what they deem acceptable and unacceptable. As well as every roleplayer can be new or old.<br>
            <br>
            Our goal with applications isn't to put ourselves higher than you, but merely have the purpose of screening. Are you compatible with our community? 

            <br>
            <br>
            What we look at is your knowledge of the lore. Your enthusiasm for being part of our community. Your viewpoint on roleplay and the Warcraft lore. And whether you have read the Heartwing-specific lore segments
            <br><br>We really enjoy roleplay, but more than anything we're a community. People who only cares about the one and not the both, will see Heartwing isn't for them.
            
            <br>
            <br>
            If your application is judged denied, it isn't an incentive to that you can never apply again. <br>
            Although please be reasonable if your application gets denied five times, we're only two people reviewing these.
            <br>
            <br>
            This is what we want you to include when sending us an application. It gives us an overview of your character and you as a person. Follow the template to the best of your ability. <br /><br />
            Disclaimer: <br />
            <ul>
            <li>We currently accept these dragonflights: Red, Green, Blue, Bronze &amp; Netherwing</li><br />

            <li>Most needed dragonflights: Green &amp; Bronze </li><br />

            <li>We do not allow corrupted dragons, custom lore dragons (shadow/light dragons), chromatic dragons, elemental drakes (and similar), undead dragons, Storm Dragonflight or Twilight Dragonflight.</li><br />

            <li>What you see in the options in the application is what you can use</li><br>


            <li>First character needs to be <strong>under</strong> the age of <span style="color: green;">500</span></li><br />

            <li>Wyrms and the older echelon of roles within the guild is more scrutinized to make sure it doesn't infringe on roleplay</li><br />

            <li>Bronze Dragons are asked to remember they don't control time, but merely can temper time on an radius basis and not on an universe-scale</li><br>

            <li>Please keep in mind that Discord usage is required, as it is our primary communication platform</li>
            <br>
            <li>Dragons over the age of 1500 needs to be coordinated and communicated with the administration. (This does not pertain to the Blue Dragonflight where all applications save for exceptions made by the administration must be below the age of 500)</li>
            </ul>
            </div>
<?php 
    switch($app){
        case 'dragon':
            $_SESSION['app'] == 1;
?>
<img src="/images/ysera-art.jpg" alt="Ysera" />
<br /><br /><br />
        
        <form method="post" action="{PROCESS_APPFORM}" id="appform"> 
        

    <div class="row">
        <label>Mortal &amp; Dragon Screenshot</label>
        <input type="url" name="image" placeholder="Use a direct link to an image" required /><br />
    </div>

    <div class="row">
        <label>Dragonflight</label>
        <select id="df" name="dragonflight" required>
                <option value="Red Dragonflight">Red Dragonflight</option>
                <option value="Green Dragonflight">Green Dragonflight</option>
                <option value="Blue Dragonflight">Blue Dragonflight</option>
                <option value="Bronze Dragonflight">Bronze Dragonflight</option>
                <option value="Netherwing">Netherwing</option>
        </select><br />
        <input type="hidden" value="red" id="dfcolor" name="dfcolor"/>
    </div>

    <div class="row">
        <label>Birth name/Dragon Name</label>
        <input type="text" name="birthname" required/><br />
    </div>

    <div class="row">
        <label>Aliases</label>
        <input type="text" name="alias" /><br />
    </div>

    <div class="row">
        <label>Age</label>
        <input type="number" id="age" name="age" required /><br />
        <div style="display: none; color: red;" id="age-notice">* Disclaimer: Your first character is not allowed to be above the age of 500!</div>
    </div>

    <div class="row">
        <label>Gender</label>
        <select name="gender" required>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
        </select><br />
    </div>

    <div class="row">
        <label>Adulthood stage</label>
        <select name="adulthood" required>
                <option value="Whelp">Whelp</option>
                <option value="Drake">Drake</option>
                <option value="Matured Dragon">Matured Dragon</option>
                <option value="Wyrm">Wyrm</option>
        </select><br />
        <div style="color: red;">* Disclaimer: We do not allow Elder Wyrms <br>(without special agreement with the Guild Leader). <br><a target="_blank" href="http://heartwing.dk/viewtopic.php?f=12&t=584&p=1594#p1594">Look here for our aging policy</a></div>
    </div>

    <div class="row">
        <label>Loyalty(-ies)</label>
        <input type="text" name="loyalties" /><br />
    </div>

    <div class="row">
        <label>Your character's view of Heartwing</label>
        <textarea name="view" required /></textarea><br />
    </div>

    <div class="row">
        <label>Affiliations</label>
        <input type="text" name="affiliations" /><br />
    </div>

    <div class="row">
        <label>Attitude/Behaviour</label>
        <textarea name="attitude" /></textarea><br />
    </div>

    <div class="row">
        <label>Key traits</label>
        <input type="text" name="traits" /><br />
    </div>

    <div class="row">
        <label>Respect for authority</label>
        <textarea name="authority" required /></textarea><br />   
    </div>  

    <div class="row">
        <label>Describe your character's personality</label>
        <textarea name="characterpersonality" required /></textarea><br />
    </div>

    <div class="row">
        <label>Describe your own personality</label>
        <textarea name="irlpersonality" /></textarea><br />
    </div>

    <div class="row">
        <label>Character occupation</label>
        <input type="text" name="occupation" /><br />
    </div>

    <div class="row">
        <label>Your character's hobbies</label>
        <input type="text" name="hobbies" /><br />
    </div>

    <div class="row">
        <label>Future desired position of your character within Heartwing?</label>
        <input type="text" name="position" /><br />
    </div>

    <div class="row">
        <label>Known/frequently/commonly used abilities/spells</label>
        <input type="text" name="common" required /><br /> 
    </div>

    <div class="row">
        <label>Special/prestige spells/abilities of your character</label>
        <input type="text" name="prestige" required /><br />
    </div> 

    <div class="row">
        <label>Summary of backstory</label>
        <textarea name="backstory" required /></textarea>
        <div style="color: red;">* Disclaimer: Please remember the higher the age you've entered. The more backstory we expect.</div>
        <br />
    </div>

     <div class="row">
        <label>Is this your first time at dragon roleplay, or do you have experience?:</label>
        <input type="text" name="experience" required /><br />
    </div> 

    <div class="row">
        <label>Your character's reason to join Heartwing:</label>
        <textarea name="ic-reason" required /></textarea><br />
    </div>

    <div class="row">
        <label>(OOC) What is your reason to join Heartwing:</label>
        <textarea name="ooc-reason" /></textarea><br />
    </div>

     <div class="row checkbox-row">
        <label>Are you aware of the guild's rules, the content of the Tome of Ancient Times forum and the lore of your dragonflight?</label>
        <input type="text" name="understand" required /><br />
    </div> 

     <div class="row checkbox-row">
        <label>Have you understood Heartwing's current timeline?:</label>
        <input type="text" name="timeline" required /><br />
    </div> 

     <div class="row checkbox-row">
        <label>Do you fully understand your dragonflight's lore, and other essential lore information:</label>
        <input type="text" name="lore" required /><br />
    </div> 

        
    <div class="row row-submit">
        <input type="submit" name="submit" id ="submit" value="{L_SUBMIT}" class="button1" />
    </div>
       
        </span>
      
   </div>

   <span class="corners-bottom"><span></span></span></div>
</div>
<?php 
    break;

    case 'mortal': 
    ?>
    Hello
    <?php
    break;
} ?>
    

<script>
JSON TEST 



var formData = JSON.stringify($("#appform").serializeArray());
console.log(JSON.stringify({ duration: item.duration, start: item.start }));
var matchingResults = JSON['data'].filter(function(x){ return x.id == 2; });

</script>



<!-- INCLUDE overall_footer.html --> 