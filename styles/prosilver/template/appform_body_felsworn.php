
<html>
<head>    
<script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>
<link href="styles/prosilver/theme/felsworn/felsworn.css" rel="stylesheet" />
</head>


<body>

<section id="intro">

    <img src="styles/prosilver/theme/felsworn/images/logo.png" alt="Logo" />


    <img src="styles/prosilver/theme/felsworn/images/Fel-Dragon.png"  class="felsworn-dragon" alt="Felblood" />
</section>


<section class="section-content" id="story">
<div class="container">
<h2>Story</h2>
<h1>The Universe is Flawed</h1>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet erat vel est malesuada ornare. In tincidunt varius eleifend. Etiam lobortis suscipit quam a aliquam. Donec molestie, lectus vel tempor vestibulum, felis nunc tristique ligula, at luctus ex est in urna. Nunc lectus sapien, consectetur vitae eleifend eu, aliquam nec neque. Nulla tellus odio, volutpat eu odio nec, imperdiet auctor arcu. Praesent pharetra enim ac tortor rhoncus semper. Morbi ut est elit. Proin at luctus nisi, a sodales eros. Sed nec lectus ut sapien mattis euismod hendrerit euismod libero. Pellentesque eget eros leo. Suspendisse sit amet diam a risus accumsan sollicitudin. Etiam non tortor ut mi vestibulum feugiat. Nullam dapibus lorem sed dolor varius sagittis.</p>

<p>Nam eget eros consectetur, accumsan libero et, efficitur lorem. Quisque consequat diam in diam rutrum, vitae pulvinar mauris ultrices. Donec efficitur elit ac ante interdum, pharetra ultrices risus blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed sed euismod magna. Integer faucibus arcu auctor tincidunt elementum. Maecenas imperdiet ex eu massa dictum ultrices. Maecenas semper vitae leo et fermentum. Morbi dictum sapien sed lacus bibendum, nec congue diam rhoncus.
</p>
</div>
</section>


<section class="section-content" id="storyline">
<div class="container">
<h2>Personal Storyline</h2>
<h1>Cast Off The Shackles by <br>The Child Within</h1>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet erat vel est malesuada ornare. In tincidunt varius eleifend. Etiam lobortis suscipit quam a aliquam. Donec molestie, lectus vel tempor vestibulum, felis nunc tristique ligula, at luctus ex est in urna. Nunc lectus sapien, consectetur vitae eleifend eu, aliquam nec neque. Nulla tellus odio, volutpat eu odio nec, imperdiet auctor arcu. Praesent pharetra enim ac tortor rhoncus semper. Morbi ut est elit. Proin at luctus nisi, a sodales eros. Sed nec lectus ut sapien mattis euismod hendrerit euismod libero. Pellentesque eget eros leo. Suspendisse sit amet diam a risus accumsan sollicitudin. Etiam non tortor ut mi vestibulum feugiat. Nullam dapibus lorem sed dolor varius sagittis.</p>

<p>Nam eget eros consectetur, accumsan libero et, efficitur lorem. Quisque consequat diam in diam rutrum, vitae pulvinar mauris ultrices. Donec efficitur elit ac ante interdum, pharetra ultrices risus blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed sed euismod magna. Integer faucibus arcu auctor tincidunt elementum. Maecenas imperdiet ex eu massa dictum ultrices. Maecenas semper vitae leo et fermentum. Morbi dictum sapien sed lacus bibendum, nec congue diam rhoncus.
</p>
</div>
</section>

<section class="section-content" id="antagonist">
<div class="container">
<h2>Become An Antagonist</h2>
<h1>His Blood Is Now Yours</h1>


<img src="styles/prosilver/theme/felsworn/images/helius-portrait.png" alt="Helius" />

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet erat vel est malesuada ornare. In tincidunt varius eleifend. Etiam lobortis suscipit quam a aliquam. Donec molestie, lectus vel tempor vestibulum, felis nunc tristique ligula, at luctus ex est in urna. Nunc lectus sapien, consectetur vitae eleifend eu, aliquam nec neque. Nulla tellus odio, volutpat eu odio nec, imperdiet auctor arcu. Praesent pharetra enim ac tortor rhoncus semper. Morbi ut est elit. Proin at luctus nisi, a sodales eros. Sed nec lectus ut sapien mattis euismod hendrerit euismod libero. Pellentesque eget eros leo. Suspendisse sit amet diam a risus accumsan sollicitudin. Etiam non tortor ut mi vestibulum feugiat. Nullam dapibus lorem sed dolor varius sagittis.</p>

<p>Nam eget eros consectetur, accumsan libero et, efficitur lorem. Quisque consequat diam in diam rutrum, vitae pulvinar mauris ultrices. Donec efficitur elit ac ante interdum, pharetra ultrices risus blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed sed euismod magna. Integer faucibus arcu auctor tincidunt elementum. Maecenas imperdiet ex eu massa dictum ultrices. Maecenas semper vitae leo et fermentum. Morbi dictum sapien sed lacus bibendum, nec congue diam rhoncus.
</p>
</div>
</section>


<section class="panel felsworn-application application-form">
    <div class="container">
   <div class="inner"><span class="corners-top"><span></span></span>

   <div class="content">
      
        <h2>Felsworn Application</h2>
    </div>
<br /><br /><br />
        
        <form method="post" action="{PROCESS_APPFORM}" id="appform"> 
    <div class="row">
        <label>Dragonflight</label>
        <select id="race" name="race" disabled required>
                <option value="Felblood">Felblood Dragon</option>
        </select><br />
        <input type="hidden" value="red" id="color" name="color"/>
    </div>
      

    <div class="row">
        <label>Mortal Screenshot</label>
        <input type="url" name="image" placeholder="Use a direct link to an image" required /><br />
    </div>

    <div class="row">
        <label>Dragon Screenshot</label>
        <input type="url" name="image" placeholder="Use a direct link to an image" required /><br />
    </div>

    <div class="row">
        <label>Dragon Name</label>
        <input type="text" name="birthname" required/><br />
    </div>

    <div class="row">
        <label>Age</label>
        <input type="number" id="age" name="age" required /><br />
    </div>

    <div class="row">
        <label>Gender</label>
        <select name="gender" required>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
        </select><br />
    </div>

    <div class="row">
        <label>Adulthood stage</label>
        <select name="adulthood" required>
                <option value="Whelp">Whelp</option>
                <option value="Drake">Drake</option>
                <option value="Matured Dragon">Matured Dragon</option>
                <option value="Wyrm">Wyrm</option>
        </select><br />
    </div>

    <div class="row">
        <label>Loyalty(-ies)</label>
        <input type="text" name="loyalties" /><br />
    </div>

    <div class="row">
        <label>Your character's view of Heartwing</label>
        <textarea name="view" required /></textarea><br />
    </div>

    <div class="row">
        <label>Affiliations</label>
        <input type="text" name="affiliations" /><br />
    </div>

    <div class="row">
        <label>Attitude/Behaviour</label>
        <textarea name="attitude" /></textarea><br />
    </div>

    <div class="row">
        <label>Key traits</label>
        <input type="text" name="traits" /><br />
    </div>

    <div class="row">
        <label>Respect for authority</label>
        <textarea name="authority" required /></textarea><br />   
    </div>  

    <div class="row">
        <label>Describe your character's personality</label>
        <textarea name="characterpersonality" required /></textarea><br />
    </div>

    <div class="row">
        <label>Describe your own personality</label>
        <textarea name="irlpersonality" /></textarea><br />
    </div>

    <div class="row">
        <label>Character occupation</label>
        <input type="text" name="occupation" /><br />
    </div>

    <div class="row">
        <label>Your character's hobbies</label>
        <input type="text" name="hobbies" /><br />
    </div>

    <div class="row">
        <label>Future desired position of your character within Heartwing?</label>
        <input type="text" name="position" /><br />
    </div>

    <div class="row">
        <label>Known/frequently/commonly used abilities/spells</label>
        <input type="text" name="common" required /><br /> 
    </div>

    <div class="row">
        <label>Special/prestige spells/abilities of your character</label>
        <input type="text" name="prestige" required /><br />
    </div> 

    <div class="row">
        <label>Summary of backstory</label>
        <textarea name="backstory" required /></textarea>
        <div style="color: red;">* Disclaimer: Please remember the higher the age you've entered. The more backstory we expect.</div>
        <br />
    </div>

     <div class="row">
        <label>Is this your first time at dragon roleplay, or do you have experience?:</label>
        <input type="text" name="experience" required /><br />
    </div> 

    <div class="row">
        <label>Your character's reason to join Heartwing:</label>
        <textarea name="ic-reason" required /></textarea><br />
    </div>

    <div class="row">
        <label>(OOC) What is your reason to join Heartwing:</label>
        <textarea name="ooc-reason" /></textarea><br />
    </div>

     <div class="row checkbox-row">
        <label>Are you aware of the guild's rules, the content of the Tome of Ancient Times forum and the lore of your dragonflight?</label>
        <input type="text" name="understand" required /><br />
    </div> 

     <div class="row checkbox-row">
        <label>Have you understood Heartwing's current timeline?:</label>
        <input type="text" name="timeline" required /><br />
    </div> 

     <div class="row checkbox-row">
        <label>Do you fully understand your dragonflight's lore, and other essential lore information:</label>
        <input type="text" name="lore" required /><br />
    </div> 

        
    <div class="row row-submit">

        <button type="submit" class="fancy-legion-btn Button">
						<div class="Button-left"></div>
	<div class="Button-middle"></div>
	<div class="Button-right"></div>
	<div class="Button-light"></div>
	<div class="Button-text">{L_SUBMIT}</div></button>
    </div>
       
        </span>
      
   </div>
</section>
</div>
<?php 
} ?>
    

<script>
JSON TEST 



var formData = JSON.stringify($("#appform").serializeArray());
console.log(JSON.stringify({ duration: item.duration, start: item.start }));
var matchingResults = JSON['data'].filter(function(x){ return x.id == 2; });

</script>



<!-- INCLUDE overall_footer.html --> 