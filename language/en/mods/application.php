<?php
/**
*
* application [English]
*
* @package language
* @copyright (c) Jim http://beta-garden.com 2009
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	'LOGIN_APPLICATION_FORM'		=> 'You need to login before you can fill out an application.',
	'APPLICATION_SUBJECT'			=> '[Dragon] %2$s',
	'APPLICATION_MESSAGE'			=> '
										[center][img]%1$s[/img][/center]
										[b][color=%4$s]Dragonflight:[/color][/b] %3$s
										[b][color=%4$s]Dragon Name:[/color][/b] %2$s
										[b][color=%4$s]Aliases:[/color][/b] %5$s
										[b][color=%4$s]Age:[/color][/b] %6$s 
										[b][color=%4$s]Gender:[/color][/b] %7$s 
										[b][color=%4$s]Adulthood:[/color][/b] %8$s 
										[b][color=%4$s]Loyalty(-ies):[/color][/b] %9$s 
										[b][color=%4$s]View of Heartwing:[/color][/b] %10$s 
										[b][color=%4$s]Affiliations:[/color][/b] %11$s 
										<br />
										[b][color=%4$s]Attitude/Behaviour:[/color][/b] %12$s 
										[b][color=%4$s]Key traits:[/color][/b] %13$s 
										[b][color=%4$s]Respect for authority:[/color][/b] %14$s 
										[b][color=%4$s]Character Personality:[/color][/b] %15$s 
										[b][color=%4$s]IRL-Person Personality:[/color][/b] %16$s 
										[b][color=%4$s]Hobbies:[/color][/b] %17$s 
										[b][color=%4$s]Occupation:[/color][/b] %18$s 
										[b][color=%4$s]Future desired position:[/color][/b] %19$s 
										[b][color=%4$s]Common spells:[/color][/b] %20$s 
										[b][color=%4$s]Prestige spells:[/color][/b] %21$s 
										[b][color=%4$s]Backstory Summary:[/color][/b] %22$s 
										[b][color=%4$s]First time at dragon roleplay?[/color][/b] %23$s 
										[b][color=%4$s](IC) Reason to join:[/color][/b] %24$s 
										[b][color=%4$s](OOC) Reason to join:[/color][/b] %25$s 
										[b][color=%4$s]Are you aware of the guild rules, the content of the Tome of Ancient Times forum and the lore of your dragonflight?:[/color][/b] %26$s 
										[b][color=%4$s]Have you understood current timeline of Heartwing?:[/color][/b] %27$s 
										[b][color=%4$s]Do you fully understand the lore of your dragonflight, and other essential lore information:[/color][/b] %28$s 
										<br />
										',
	'APPLICATION_SUBJECT_MORTAL'			=> '[MORTAL] %2$s',
	'APPLICATION_MESSAGE_MORTAL'			=> '
										[center][img]%1$s[/img][/center]<br>
										[b][color=#C885FD]Name:[/color][/b] %2$s
										[b][color=#C885FD]Race:[/color][/b] %3$s
										[b][color=#C885FD]Gender:[/color][/b] %4$s
										[b][color=#C885FD]Class:[/color][/b] %5$s
										[b][color=#C885FD]Age:[/color][/b] %6$s
										[b][color=#C885FD]Dragonsworn?:[/color][/b] %7$s
										[b][color=#C885FD]Dragonflight Alignment: [/color][/b] %8$s
										[b][color=#C885FD]Physique Type:[/color][/b] %9$s
										[b][color=#C885FD]Choice of magic:[/color][/b] %10$s
										[b][color=#C885FD]Language(s):[/color][/b] %11$s
										[b][color=#C885FD]Character Personality: [/color][/b] %12$s
										[b][color=#C885FD]IRL Personality: [/color][/b] %13$s
										[b][color=#C885FD]Interest(s): [/color][/b] %14$s
										[b][color=#C885FD]Minor spell(s): [/color][/b] %15$s
										[b][color=#C885FD]Prestige spell(s): [/color][/b] %16$s
										[b][color=#C885FD]Backstory: [/color][/b] %17$s
										[b][color=#C885FD](IC) How much does your mortal know about dragons?: [/color][/b] %18$s
										[b][color=#C885FD](OOC) How much does you know about dragon lore?: [/color][/b] %19$s
										[b][color=#C885FD](IC) Reason to join [/color][/b] %20$s
										[b][color=#C885FD](OOC) Reason to join [/color][/b] %21$s
										[b][color=#C885FD]What other roleplay have you done before this character? [/color][/b] %22$s
										[b][color=#C885FD]Are you aware of the guild rules, the content of the Tome of Ancient Times forum and the lore of your dragonflight?:[/color][/b] %23$s 
										[b][color=#C885FD]Have you understood current timeline of Heartwing?:[/color][/b] %24$s 
										[b][color=#C885FD]Do you fully understand the lore of your dragonflight, and other essential lore information:[/color][/b] %25$s 
										<br />
										,
	
										',
	'APPLICATION_SUBJECT_DRAGONSPAWN'			=> '[DRAGONSPAWN] %2$s',
	'APPLICATION_MESSAGE_DRAGONSPAWN'			=> '
										[center][img]%1$s[/img][/center]<br>
										[center][img]%2$s[/img][/center]<br>
										[b][color=%4$s]Dragonflight:[/color][/b] %3$s
										[b][color=%4$s]{Optional} Pre-Dragonspawn Nam:[/color][/b] %5$s
										[b][color=%4$s]Post-Dragonspawn Name:[/color][/b] %6$s
										[b][color=%4$s]Gender:[/color][/b] %7$s
										[b][color=%4$s]Was your character born dragonspawn or later in life became one?:[/color][/b] %8$s
										[b][color=%4$s]Loyalty(-ies): [/color][/b] %9$s
										[b][color=%4$s]Age:[/color][/b] %10$s
										[b][color=%4$s]Your characters view/opinion of Heartwing:[/color][/b] %11$s
										[b][color=%4$s]Who or which organizations is your character connected with?:[/color][/b] %12$s
										[b][color=%4$s]Respect for authority: [/color][/b] %13$s
										[b][color=%4$s]Describe your characters personality: [/color][/b] %14$s
										[b][color=%4$s]Describe your own personality: [/color][/b] %15$s
										[b][color=%4$s]What does your character interest himself/herself in?: [/color][/b] %16$s
										[b][color=%4$s]{Optional} Summary of backstory - before your character became a dragonspawn: [/color][/b] %17$s
										[b][color=%4$s]Summary of backstory as dragonspawn: [/color][/b] %18$s
										[b][color=%4$s]Minor spells: [/color][/b] %19$s
										[b][color=%4$s]Prestige spells: [/color][/b] %20$s
										[b][color=%4$s]Is this your first time at dragon/dragonspawn roleplay, or do you have experience?: [/color][/b] %21$s
										[b][color=%4$s]Have you fully read this guild thread, and are fully aware of the guilds rules and content?: [/color][/b] %22$s
										[b][color=%4$s]Have you understood Heartwings current timeline?: [/color][/b] %23$s										
										[b][color=%4$s]Your characters reason to join Heartwing: [/color][/b] %24$s 
										[b][color=%4$s](OOC) What is your reason to join Heartwing: [/color][/b] %25$s 
										[b][color=%4$s]What other roleplay have you done before this character?: [/color][/b] %26$s 
										<br />

										',
	'APPLICATION_SEND'				=> 'Your application has been sent to the administrators of this board. They will determine if your application meets guild standards and get back to you in the coming days.',
	'APPLICATION_PAGETITLE'			=> 'Application form',
	
	'APPLICATION_WELCOME_MESSAGE'	=> '',
	'APPLICATION_REALNAME'			=> '',
	'APPLICATION_EMAIL'				=> '',
	'APPLICATION_POSITION'			=> '',
	'APPLICATION_TEAM1'				=> '',
	'APPLICATION_TEAM2'				=> '',
	'APPLICATION_TEAM3'				=> '',
	'APPLICATION_WHY'				=> '',
));

?>