<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Project Demonblood</title>
<link href="layout.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
</head>
<body>
  <main>
      <h2>Project:</h2>
      <h1>Demonblood</h1>
      <section class="info">
        <div class="container">
          <h3>Information</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique sollicitudin vestibulum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam dapibus blandit elit, nec ullamcorper mauris suscipit eget. Nunc commodo leo eros, eget dignissim nunc egestas quis. Curabitur pharetra accumsan nibh at ultricies. Sed volutpat finibus justo. Morbi ultrices est nec porta laoreet. Vestibulum bibendum dolor vitae ex dictum, sed rutrum nulla facilisis. Fusce gravida ac quam eget cursus. Quisque ultrices elit tortor, posuere iaculis eros luctus sed.</p>

          <p>Ut lacus purus, dapibus vitae elementum quis, semper in lacus. Etiam rhoncus aliquam lorem, ac posuere metus placerat at. In ultrices nec nunc a maximus. Integer varius, mauris nec placerat commodo, orci ex dignissim lacus, vel gravida erat diam et risus. Aenean accumsan, risus sed facilisis ultricies, lectus risus tristique augue, et dignissim justo urna ac nulla. Sed justo nulla, feugiat vel euismod a, fermentum vitae erat. Fusce vitae magna quis nibh laoreet semper. Sed at massa rutrum, elementum libero sed, blandit erat.</p>
          </div>
      </section>
      <section class="characters helius">
        <div class="container">
          <ul>
              <li>Sargeras</li>
              <li>Heliustrasz</li>
              <li>Vyraestrasza</li>
          </ul>
          <div class="character-information">
            <div class="left-info">
              <img src="/bio/images/Aeren-avatar.gif" alt="Aeren">
            </div>
            <div class="right-info">
              <h4>Heliustrasz</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tristique sollicitudin vestibulum. Suspendisse potenti. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam dapibus blandit elit, nec ullamcorper mauris suscipit eget. Nunc commodo leo eros, eget dignissim nunc egestas quis. Curabitur pharetra accumsan nibh at ultricies. Sed volutpat finibus justo. Morbi ultrices est nec porta laoreet. Vestibulum bibendum dolor vitae ex dictum, sed rutrum nulla facilisis. Fusce gravida ac quam eget cursus. Quisque ultrices elit tortor, posuere iaculis eros luctus sed.</p>
            </div>
          </div>
        </div>
      </section>
  </main>
  <footer>
      <p>All images belong to Blizzard Entertainment</p>
  </footer>
</body>
</html>
