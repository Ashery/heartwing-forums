<?php $Content=  $_GET['Content']; // change the case # ?>
<!DOCTYPE html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Project Demonblood</title>
    
    <link defer="defer" rel="stylesheet" href="stylesheet.css" type="text/css">
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Exo' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/parallax.js"></script>
    <!-- Grid -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet" type="text/css">
</head>
<script>
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script>
<body>
    <nav>
      <ul>
        <li><a href="#cata">Cata</a></li>
        <li><a href="#mop">MoP</a></li>
        <li><a href="#chrs">Characters</a></li>
      </ul>
    </nav>
    <main>
      <section id="cata">
        <video playsinline autoplay muted loop poster="deathwing.jpg" id="bgvid">
            <source src="video/Deathwing.webm" type="video/webm">
            <source src="video/Deathwing.mp4" type="video/mp4">
        </video>
        <div id="text-intro">
          <h1>You assured Deathwing's Defeat</h1>
          <p>Because of you. Deathwing has been annihilated from reality. <br />By helping to assure Katherina's downfall, you assured the Heroes', <br >the Dragon-Aspects and Go'el's victory over the Destroyer
          </p>

      </section>
      <section id="mop">
        <video playsinline autoplay muted loop poster="mop.jpg" id="bgvid">
            <source src="video/MoP.webm" type="video/webm">
            <source src="video/MoP.mp4" type="video/mp4">
        </video>
        <div id="text-intro">
          <h2>Next Chapter:</h2>
          <h1>Venture into A Myth</h1>
          <p>Your Dragonflight is infertile. Your powers are diminished. <br />Trying to find your way through the world, Heartwing will stumble upon a <br >land mostly forgotten to find out their origination and possibly their future.
          </p>

      </section>
      <section id="chrs">

  <div class="characters" id="characters-panel">
      <?php
        switch ($Content)
        {
          case one:
          include 'inc/aeren.php';
          break;
          
          case two:
          include 'inc/azrael.php';
          break;
          
          case three:
          include 'inc/helius.php';
          break;

          case four: 
          include 'inc/vyrae.php';
          break;

          case five:
          include 'inc/orodormu.php';
          break;

          case six:
          include 'inc/veri.php';
          break;

          case seven: 
          include 'inc/malphaezar.php';
          break;



          default:
          include 'inc/azrael.php';
          }
      ?>
    </div>
    <nav>
    <ul>
      <li>
        <a href="index.php?Content=two">Azraelstrasz</a>
      </li>
      <li>
        <a href="index.php?Content=three">Helius</a>
      </li>
      <li>
        <a href="index.php?Content=four">Vyraestrasza</a>
      </li>
      <li>
        <a href="index.php?Content=one">Lord Aerenstrasz</a>
      </li>
      <li>
        <a href="index.php?Content=five">Orodormu</a>
      </li>
      <li>
        <a href="index.php?Content=six">Veristrasza</a>
      </li>
      <li>
        <a href="index.php?Content=seven">Malphaezar</a>
      </li>
    </ul>
  </nav>
  </section>
  </div>
  </div>
      </main>
      <footer style="text-align: center; ">
        All graphic material belongs to Blizzard Entertainment
      </footer>
      </div>
    </div>
</body>
</html>