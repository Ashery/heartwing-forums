  <div class="character-section red-bg">
    <div class="wrapper">
        <div class="left col-6">
        <img class="player" src="http://heartwing.dk/bio/images/Aeren-avatar.gif" />
        </div>
        <div class="right col-6">
          <div>
          <h3>Lord Aerenstrasz</h3>
          <p>Burdened with an too early ascension to the mantle of leadership within Heartwing, Aeren needs to figure out his, his children’s and Heartwing’s stead in this world who no longer needs draconic guardians. Having to fill the giant shoes that his twin-brother Hydra and his father, Asherystrasz left him, he is trying his utmost to find out why the Pantheon ascended the dragonflights, knowing full well that Helius is on the horizon, but not every enemy is fueled by felblood, and Aeren must choose which family he belongs to; his blood or Heartwing.
          </p>
        </div>
        </div>
      </div>
    </div>
  </div>
  </div>