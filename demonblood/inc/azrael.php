  <div class="character-section azrael azrael-bg red-bg">
    <div class="wrapper">
        <div class="left col-6">
        <img class="player" style="max-width: 92%;         margin-left: 111px; "src="images/Azrael-avatar.gif" />
        </div>
        <div class="right col-6">
          <div>
          <h3>Lord Azraelstrasz Tyranistres</h3>
          <p><i>“It is not the means we use, but the purity of our intentions that define us”</i> <br /><br />
          Azrael owes allegiance to nobody but himself and his family. Azrael has always been estranged to the others of his flight, always trying to delve 
          deeper into the meaning that Eonar meant to convey. He constitutes the ideals of the Red Dragonflight as misinterpretations, instead, Azrael employs 
          the powers of Life and Death equally. He never hesitates to use whatever means necessary to complete his goal, and with his Father’s death, Azrael is 
          now the Head of the Tyranistres family.
          </p>
        </div>
        </div>
      </div>
    </div>
  </div>
  </div>