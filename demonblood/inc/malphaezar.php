  <div class="character-section malph green-bg">
    <div class="wrapper">
        <div class="left col-6">
        <img class="player" style="max-width: 105%;     margin-left: 72px; "src="images/Malphaezar-avatar.gif" />
        </div>
        <div class="right col-6">
          <div>
          <h3>Malphaezar</h3>
          <p>Born on the nathrezim homeworld of Nathreza, Malphaezar in tow of the Legion’s plans was leading the forces of a major sect in the 
            Burning Legion. 7,000 years ago, his position and rank was taken from him by none other than Heliustrasz. With blade to neck, 
            Helius spared Malphaezar’s life with the compromise that he would now serve him and Malphaezar’s army would no longer be his. 
            Left alone on Azeroth, after Helius left into the Great Dark. Malphaezar is acting on old orders from the Cataclysmic War; 
            To eradicate all dragons.
          </p>
        </div>
        </div>
      </div>
    </div>
  </div>
  </div>