  <div class="character-section vyrae green-bg">
    <div class="wrapper">
        <div class="left col-6">
        <img class="player" style="max-width: 84%;
    margin-left: 164px; "src="images/Vyrae-avatar.gif" />
        </div>
        <div class="right col-6">
          <div>
          <h3 style="line-height: 1.4">Vyraestrasza</h3>
          <p>Hydrastrasz, former leader of the Heartwing forces was the true love of Vyraestrasza. Having lived with a century of a one-sided romance,
           Vyrae’s rage grew over the years. When Hydra left Vyrae in exchange for Adiastrasza, she was thrown into a deep pit of depression. 
           Having already mated with her now former lover, she tried to take her own eggs from the Chambers of the Father, only to be put down and 
           felled by Asherystrasz. Now, Helius has raised her unresisting body and soul from the corners of the Shadowlands to lead his forces in undeath. 
          </p>
        </div>
        </div>
      </div>
    </div>
  </div>
  </div>