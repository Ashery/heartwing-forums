  <div class="character-section vyrae green-bg">
    <div class="wrapper">
        <div class="left col-6">
        <img class="player" style="
    max-width: 92%;
    margin-left: 54px;
    margin-top:  -34px;
    " src="images/Helius.gif">
        </div>
        <div class="right col-6">
          <div>
          <h3 style="line-height: 1.4">Helius</h3>
          <p>Helius cares for none but himself, serving the Burning Legion only because it was his last resort during the War of the Ancients. 
            Amidst the war, the behemoth of fel bred energies that became of Helius almost took down Asherystrasz and the entire family of Tyranistres. 
            Though he was eventually defeated at the hands of his father, it only unveiled just one deeper cruelty within the pact he had made with 
            his demon masters; Helius had ascended to immortality.
          </p>
        </div>
        </div>
      </div>
    </div>
  </div>
  </div>