<?php
/**
*
* @package phpBB3 Enable HTML
* @copyright (c) 2008 EXreaction, Lithium Studios
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/


/**
* @ignore
*/
define('UMIL_AUTO', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
if (!file_exists($phpbb_root_path . 'umil/umil_auto.' . $phpEx))
{
	trigger_error('Please download the latest UMIL (Unified MOD Install Library) from: <a href="http://www.phpbb.com/mods/umil/">phpBB.com/mods/umil</a>', E_USER_ERROR);
}

define('IN_PHPBB', true);
include($phpbb_root_path . 'common.' . $phpEx);
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/enable_html');

// This is required because versions prior to 1.2.3 did not have a version number, so we need to check if it was installed and then set the version number if required.
if (!file_exists($phpbb_root_path . 'umil/umil.' . $phpEx))
{
	trigger_error('Please download the latest UMIL (Unified MOD Install Library) from: <a href="http://www.phpbb.com/mods/umil/">phpBB.com/mods/umil</a>', E_USER_ERROR);
}
include($phpbb_root_path . 'umil/umil.' . $phpEx);
$umil = new umil(true);
if (!$umil->config_exists('enable_html_version') && $umil->permission_exists('u_html'))
{
	$umil->config_add('enable_html_version', '1.2.2');
}
else if (!$umil->config_exists('enable_html_version') && $umil->permission_exists('f_html'))
{
	$umil->config_add('enable_html_version', '1.0.0');
}

$mod_name = 'ENABLE_HTML';
$version_config_name = 'enable_html_version';

$versions = array(
	'1.0.0'		=> array(
		'permission_add' => array(
			array('f_html', false),
		),
	),
	'1.0.1'		=> array(
		'permission_add' => array(
			'u_html',
		),

		'cache_purge' => array(
			'',
		),
	),
	'1.2.0'		=> array(),
	'1.2.1'		=> array(),
	'1.2.2'		=> array(),
	'1.2.3'		=> array(),
);

include($phpbb_root_path . 'umil/umil_auto.' . $phpEx);

?>