<?php
//
//	file: language/fr/mods/info_acp_attributes.php
//	author: abdev
//	begin: 11/22/2010
//	version: 0.1.2 - 08/26/2011
//	licence: http://opensource.org/licenses/gpl-license.php GNU Public License
//

// ignore
if ( !defined('IN_PHPBB') )
{
	exit;
}

// init lang ary, if it doesn't !
if ( empty($lang) || !is_array($lang) )
{
	$lang = array();
}

// administration
$lang = array_merge($lang, array(
	'QTE_MANAGE' => 'Gérer les attributs de sujet',
	'QTE_MANAGE_TITLE' => 'Attributs de sujet',
	'QTE_MANAGE_EXPLAIN' => 'Depuis cette page vous pouvez gérer les libellés et les icônes qui seront utilisés en tant qu’attributs de sujet.',

	'QTE_ADD' => 'Ajouter un nouvel attribut',
	'QTE_ADD_EXPLAIN' => 'Vous pouvez définir ici les champs du nouvel attribut.',
	'QTE_EDIT' => 'Éditer un attribut',
	'QTE_EDIT_EXPLAIN' => 'Vous pouvez modifier ici les champs de l’attribut sélectionné.',

	'QTE_FIELDS' => 'Champs de l’attribut',
	'QTE_TYPE' => 'Type d’attribut',
	'QTE_TYPE_TXT' => 'Texte',
	'QTE_TYPE_IMG' => 'Image',
	'QTE_NAME' => 'Intitulé de l’attribut',
	'QTE_NAME_EXPLAIN' => '- Vous pouvez utiliser une clé de langue, ou saisir l’intitulé en clair.<br />- Insérer <strong>%%mod%%</strong> affichera le nom de l’utilisateur ayant appliqué l’attribut.<br />- Insérer <strong>%%date%%</strong> affichera la date du jour où l’attribut a été appliqué.<br /><br />- Example : <strong>[Réglé par %%mod%%]</strong> affichera <strong>[Réglé par %s]</strong>',
	'QTE_DESC' => 'Description de l’attribut',
	'QTE_DESC_EXPLAIN' => 'Vous pouvez entrer un commentaire court, lequel sera utilisé pour différencier vos attributs si certains doivent avoir le même nom.',
	'QTE_IMG' => 'Image de l’attribut',
	'QTE_IMG_EXPLAIN' => 'Vous pouvez utiliser une clé d’image, ou saisir le chemin relatif de l’image.',
	'QTE_DATE' => 'Format de date de l’attribut',
	'QTE_DATE_EXPLAIN' => 'La syntaxe utilisée est identique à la fonction PHP <a href="http://www.php.net/date">date()</a>.',
	'QTE_COLOUR' => 'Couleur de l’attribut',
	'QTE_COLOUR_EXPLAIN' => 'Sélectionnez une valeur dans la <strong>palette de couleurs</strong>, ou saisissez-la manuellement.<br />Laisser vide pour utiliser une classe CSS nommée comme l’attribut.',
	'QTE_USER_COLOUR' => 'Colorer le nom de l’utilisateur ayant appliqué l’attribut',
	'QTE_USER_COLOUR_EXPLAIN' => 'Si vous utilisez l’argument <strong>%mod%</strong> et que cette option est activée, la couleur du groupe de l’utilisateur sera appliquée.',

	'QTE_PERMISSIONS' => 'Permissions de l’attribut',
	'QTE_ALLOWED_FORUMS' => 'Forums autorisés',
	'QTE_ALLOWED_FORUMS_EXPLAIN' => 'Forums dont les groupes autorisés peuvent utiliser cet attribut.<br />Sélectionnez plusieurs forums en maintenant la touche <samp>CTRL</samp> ou la touche <samp>COMMAND</samp> et en cliquant.',
	'QTE_ALLOWED_GROUPS' => 'Groupes autorisés',
	'QTE_ALLOWED_GROUPS_EXPLAIN' => 'Groupes autorisés à utiliser cet attribut.<br />Sélectionnez plusieurs groupes en maintenant la touche <samp>CTRL</samp> ou la touche <samp>COMMAND</samp> et en cliquant.',
	'QTE_ALLOWED_AUTHOR' => 'Autoriser l’auteur du sujet à utiliser cet attribut dans les forums sélectionnés',
	'QTE_ALLOWED_AUTHOR_EXPLAIN' => 'Si cette option est activée, l’auteur du sujet pourra utiliser cet attribut, même s’il ne fait pas partie des groupes sélectionnés.',

	'QTE_AUTH_ADD' => 'Ajouter une permission',
	'QTE_AUTH_REMOVE' => 'Supprimer cette permission',

	'QTE_ATTRIBUTE' => 'Attribut',
	'QTE_ATTRIBUTES' => 'Attributs',
	'QTE_USAGE' => 'Utilisation',

	'QTE_CSS' => 'Probablement géré en CSS',
	'QTE_NONE' => 'N/A',
	'QTE_KEY_USERNAME' => '¦utilisateur¦',
	'QTE_KEY_DATE' => '¦date¦',

	'QTE_MUST_SELECT' => 'Vous devez sélectionner un attribut.',
	'QTE_NAME_ERROR' => 'Le champ “Intitulé de l’attribut” semble être vidé.',
	'QTE_DESC_ERROR' => 'Le champ “Description de l’attribut” semble être trop long.',
	'QTE_COLOUR_ERROR' => 'Le champ “Couleur de l’attribut” semble comporter une erreur.',
	'QTE_DATE_ARGUMENT_ERROR' => 'Vous avez défini un format de date. Hors, vous n’avez pas défini l’argument <strong>%date%</strong> dans votre attribut.',
	'QTE_DATE_FORMAT_ERROR' => 'Vous avez défini l’argument <strong>%date%</strong> dans votre attribut. Hors, vous n’avez pas défini de format de date.',
	'QTE_USER_COLOUR_ERROR' => 'Vous avez activé l’option pour colorer le nom de l’utilisateur. Hors, vous n’avez pas défini l’argument <strong>%mod%</strong> dans votre attribut.',
	'QTE_FORUM_ERROR' => 'Vous ne pouvez pas spécifier une catégorie ou un forum-lien.',

	'QTE_ADDED' => 'Un nouvel attribut a été ajouté.',
	'QTE_UPDATED' => 'L’attribut sélectionné a été mis à jour.',
	'QTE_REMOVED' => 'L’attribut sélectionné a été supprimé.',

	'LOG_ATTRIBUTE_ADDED' => '<strong>Ajout d’un nouvel attribut</strong><br />» %s',
	'LOG_ATTRIBUTE_UPDATED' => '<strong>Mise à jour d’un attribut</strong><br />» %s',
	'LOG_ATTRIBUTE_REMOVED'	=> '<strong>Suppression d’un attribut</strong><br />» %s',
	'LOG_ATTRIBUTE_MOVE_DOWN'	=> '<strong>Déplacement d’un attribut</strong> %1$s <strong>en dessous de</strong> %2$s',
	'LOG_ATTRIBUTE_MOVE_UP'	=> '<strong>Déplacement d’un attribut</strong> %1$s <strong>au dessus de</strong> %2$s',
));

// forums
$lang = array_merge($lang, array(
	'QTE_FORCE_USERS' => 'Forcer les utilisateurs à appliquer un attribut à leur sujet',
	'QTE_FORCE_USERS_EXPLAIN' => 'Si activée, les utilisateurs devront sélectionner un attribut pour leur sujet dans ce forum.',
));

// umil
$lang = array_merge($lang, array(
	'QTE' => 'Quick Title Edition',

	'INSTALL_QTE' => 'Installer Quick Title Edition',
	'INSTALL_QTE_CONFIRM' => 'Êtes-vous prêt à installer Quick Title Edition ?',
	'UPDATE_QTE' => 'Mettre à jour Quick Title Edition',
	'UPDATE_QTE_CONFIRM' => 'Êtes-vous prêt à mettre Quick Title Edition ?',
	'UNINSTALL_QTE' => 'Désinstaller Quick Title Edition',
	'UNINSTALL_QTE_CONFIRM' => 'Êtes-vous prêt à désinstaller Quick Title Edition ? Tous les réglages et données sauvegardées par ce MOD seront supprimés !',
));

// upgrades
$lang = array_merge($lang, array(
	'QTE_RC2_UPDATED' => 'Votre version de “Quick Title Edition” a été mise à jour.<br /><br />N’oubliez pas de supprimer ce fichier de votre serveur.',
	'QTE_RC2_GREATER' => 'Vous utilisez déjà une version supérieure à la version RC2.<br /><br />N’oubliez pas de supprimer ce fichier de votre serveur.',
));

// permissions
$lang = array_merge($lang, array(
	'acl_a_attr_manage' => array('lang' => 'Peut gérer les attributs de sujet', 'cat' => 'posting'),
));

// legacy
$lang = array_merge($lang, array(
	'ACP_MANAGE_ATTRIBUTES' => 'Topic attributes',
));
