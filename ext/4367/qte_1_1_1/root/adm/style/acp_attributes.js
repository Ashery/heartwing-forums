//
//	file: adm/style/acp_attributes.js
//	author: pastisd
//	begin: 11/29/2010
//	version: 0.0.4 - 12/02/2010
//	licence: http://opensource.org/licenses/gpl-license.php GNU Public License
//

$(document).ready(function() {
	$('input[name=attr_type]').change(function() {
		$('#img').slideToggle();
	});

	var show_hide_remove_link = function(count) {
		if ( count > 1 ) {
			$('.auth_remove').show();
		} else {
			$('.auth_remove').hide();
		}
	}

	var live_add = function() {
		$('.auths_add').live('click', function() {
			var clone = $('#acp_attributes fieldset:last').prev().clone();
			var count = $('#acp_attributes fieldset.auths').length;

			$('select[id^=allowed_forums]', $(clone)).attr('name', "attr_auths[" + count + "][forums_ids][]");
			$('select[id^=allowed_groups]', $(clone)).attr('name', "attr_auths[" + count + "][groups_ids][]");
			$('input[id^=allowed_author]', $(clone)).attr('name', "attr_auths[" + count + "][author]");

			$('label[for^=allowed_forums]', $(clone)).attr('for', "allowed_forums_" + count );
			$('select[id^=allowed_forums]', $(clone)).attr('id', "allowed_forums_" + count);

			$('label[for^=allowed_groups]', $(clone)).attr('for', "allowed_groups_" + count);
			$('select[id^=allowed_groups]', $(clone)).attr('id', "allowed_groups_" + count);
			
			$('label[for^=allowed_author]', $(clone)).attr('for', "allowed_author_" + count);
			$('input[id^=allowed_author]', $(clone)).attr('id', "allowed_author_" + count);

			$('select option', $(clone)).removeAttr('selected');
			$('input', $(clone)).removeAttr('checked');
			$(clone).css("display", "none");
			$('#acp_attributes fieldset:last').before(clone);
			$('.auths_add').die('click');
			$(clone).slideToggle(null, function() {
				live_add();
			});
			show_hide_remove_link(count + 1);
		});
	}

	var live_remove = function() {
		$('.auth_remove a').live('click', function() {
			$('.auth_remove a').die('click');
			$(this).parents('fieldset').slideToggle(null, function() {
				$(this).remove();
				var count = $('#acp_attributes fieldset.auths').length;
				show_hide_remove_link(count);
				live_remove();
			});
		});
	}

	live_add();
	live_remove();

});