<?php
//
//	file: adm/mods/qte_version.php
//	author: abdev
//	begin: 12/03/2010
//	version: 0.0.5 - 06/15/2012
//	licence: http://opensource.org/licenses/gpl-license.php GNU Public License
//

// ignore
if ( !defined('IN_PHPBB') )
{
	exit;
}

class qte_version
{
	function version()
	{
		return array(
			'author' => 'ABDev',
			'title' => 'Quick Title Edition',
			'tag' => 'qte',
			'version' => '1.1.1',
			'file' => array('abdev.free.fr', '', 'mods.xml'),
		);
	}
}
