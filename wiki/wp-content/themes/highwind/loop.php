<?php
/**
 * The loop template.
 * @package highwind
 * @since 1.0
 */
?>

<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php highwind_entry_before(); ?>

<div id="post-wrap" class="post-wrap">

<?php while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a class="absolute-link" href="<?php echo the_permalink(); ?>"></a>

		<?php highwind_entry_top(); ?>

		

		<a href="<?php echo the_permalink(); ?>"><span>
           <?php 
	           		if ( has_post_thumbnail() ) {
					$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
	                echo '<div class="image-bg" style="background-image:url('.$feat_image_url.');">';
	                ?>

						<div style="visibility: hidden;">
							<?php echo get_the_post_thumbnail(); ?>
						</div>
					</div>
					<?php
				    }
	         		?>

				</span></a>
		<div class="content-area">
<?php
  setup_postdata($post);
  if ( $perma_cat != null ) {
    $cat_id = $perma_cat['category'];
    $cat = get_category($cat_id);
    $category_name = $cat->name;
  } else {               
    $cat = get_the_category();
    $category_name = $cat[0]->cat_name;
  }
?>
<span class="category"><?php echo $category_name ?></span>

		<h1 class="post-title"><a href="<?php echo the_permalink(); ?>"><?php echo get_the_title(); ?></a></h1>

		<?php if (get_field('is_this_a_short_story')) { ?>
			<h3 style="color: <?php echo get_field('race'); ?>">
		<?php echo get_field('character'); ?>
			</h3>
		<?php } ?>

		<?php highwind_entry_bottom(); ?>

		</div>
	</article><!-- #post-<?php the_ID(); ?> -->

<?php endwhile; ?>

</div><!--/.post-wrap-->

<?php highwind_entry_after(); ?>