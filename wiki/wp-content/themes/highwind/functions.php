<?php
/**
 * Highwind functions and definitions
 * @package highwind
 * @since 1.0
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/**
 * Load Theme Actions
 */
require_once( get_template_directory() . '/includes/theme-actions.php' );


/**
 * Load Framework
 */
require_once( get_template_directory() . '/framework/highwind-init.php' );


/**
 * Integrations
 */
if ( is_woocommerce_activated() ) {
	require_once( get_template_directory() . '/includes/integrations/woocommerce/setup.php' );
	require_once( get_template_directory() . '/includes/integrations/woocommerce/functions.php' );
	require_once( get_template_directory() . '/includes/integrations/woocommerce/template.php' );
}
require_once( get_template_directory() . '/includes/integrations/jetpack/functions.php' );


/**
 * Custom Functions
 * Add any custom code below. To protect your changes during updates create a child theme instead.
 * http://codex.wordpress.org/Child_Themes
 */

function custom_style_sheet() {
wp_enqueue_style( 'custom-styling', get_stylesheet_directory_uri() . '/custom.css' );
}
add_action('wp_enqueue_scripts', 'custom_style_sheet');


//jvn version control
//the new version arg will look random, but is actually builded by file modifed date, and wp version
function jvn_file_version_control($src,$handle = '') {
	global $wp_version;
	//get the info of the url
	$url_info = parse_url($src);
	//remove version arg
	$clean_src = remove_query_arg( array('ver'),$src);
	//check if file is on local server / and / or exist
	if(file_exists($_SERVER["DOCUMENT_ROOT"].$url_info['path'])) {
		//get timestamp for last modifed
		$last_mod = filemtime($_SERVER["DOCUMENT_ROOT"].$url_info['path']);	
	}else {
		//file not exist, or on another server.		
		$last_mod = '1.0.0';
	}
	//include wp-version in version, because when wp is updated we should always get the browser to get new files.
	//hash wp version, so we dont flash it around.
	//include last_mod in hash, to shorten the arg
	$version = wp_hash($wp_version.'-'.$last_mod);
	$new_scr = add_query_arg('ver',$version, $src);
	
	return $new_scr;
}
add_filter( 'style_loader_src','jvn_file_version_control', 10);
add_filter( 'script_loader_src','jvn_file_version_control', 10);