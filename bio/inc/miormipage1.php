<div id="gif-container" class="miormi-container"><img src="/bio/images/miormi.gif" alt="Miormi"/></div>
<h3> Service I </h3>
<p>“I did as you asked,” the diminutive drake spoke anxiously, shuffling closer to Miormi as she gazed into the only current light source in the room – the gleaming light-statue portraying an ancient and fierce battle between dragon and giant. With the spoken words, she only turned her head away from the glaring cyan glow slightly – just enough to show that she had acknowledged anything had been said to begin with.</p>

<p>“Report.”</p>

<p>After a moment or so, the drake settled beside the gnome-form dragon and spoke very matter-of-factly, “Very few messages in or out, but she’s definitely still there.“</p>

<p>With a quiet intake of breath, she turned to take the written report from the drake’s claw, unfurling it to read while she responded, “Months after we appeared she is still there… Seems unlikely that she told them.”</p>

<p>“Told them what?” the bronze drake tilted her head in confusion, “Who is she?”</p>

<p>“I don’t know her name,” Miormi explained with a broad gesture of her hand as she migrated toward her desk and clambered into the waiting chair to survey the pile of papers waiting there, “She was spared from death by our operatives previously, and it seems she has not mentioned our operations. Honour, perhaps, or regret… Either way, she has enough second thoughts to not drone on for the Hammer.”</p>

<p>“What are you going to do?”</p>

<p>The gnome smiled thinly, striking a match and igniting a small candle underneath a filled crucible of wax, “Why, talk to her of course.”</p>

<p>“She could still be dangerous.”</p>

<p>“You are quite right Xae. But I will manage, it is worth the risk,” she nodded slowly, tipping a small amount of the now-melted wax onto an envelope and thumping the fluid wax with an elaborate titanic-script stamp to leave a tell-tale impression of her involvement, “Pass this report to our scout group in the Fjord. I will be back by the time you are done.”</p>

<p>Turning about, she began a slow and methodical waddle across the massive open floor of the complex in which she had made her home – perhaps it would be more accurate to call it an expedition or a study than a home, but a home nonetheless. A welcome retreat from the outside world, a place that she could keep more or less to herself – bar the occasional visitor or temporary resident. On her left as she went, the heavy scent of Tanaris cactus apple fragrance filled her lungs and she smiled a relaxed smile. These had been in short supply of late, given how much called her away from this lair of hers – this one place she could be certain there would be no strife or stupidity. </p>

<p>‘Particularly,’ she thought to herself, ‘The latter.’</p>

<p>Around a massive supporting pillar of the ancient temple was the bizarre contraption she had constructed for both convenient and above all secure access to her home and to the world beyond. Sand still littered the floor in small dunes, representing Xaeridormi’s often hilariously bungled attempts to make use of the portal and brought sections of the Caverns or the desert itself with her while tuning into the appropriate line. At least this time she was the one opening it, and with a terrific groan the jury-rigged contraption spun into life and the hourglasses either side of it inverted of their own accord. In the centre, a bright golden light and a twisting vortex beckoned beyond. Planting her hands on the floor, Miormi exploded into her draconic form and took to the air, beating her wings to fly toward that vortex, the rather-smaller comrade following behind.
</p>

<p>Immediately apparent was the great wave of icy cold air that swept over the dragon and drake, chilling both almost instantly as they spiralled out of the portal and soared into the skies over Northrend.</p>

<p>“Race you home, Mio!” shouted the drake, who was already veering off south-east toward the Fjord, “You’ll owe me lunch!”</p>

<p>Snapping her jaws and laughing dryly, Miormi elected not to respond and folded her wings inward, plummeting toward the ground and spreading her wings in time to slow her descent and skim across the clear coastline. Upon reaching the myriad trees of the Grizzly Hills, however, she simply vanished into the dense underbrush with little more ceremony than a sharp flash of light. From thereon, it was a leisurely hike through the forest toward the mountain range in which the little cultist outpost had been situated.</p>

<p>There she found the Tauren she had sought; dark gray fur arising out of her skin at a sharp angle in response to the stinging mountain winds. Though the magenta flame of the brazier offered some comfort, it clearly was not as much as she would have otherwise hoped. Miormi asserted that her lack of awareness for her approach was symptomatic of dulled senses from long inaction in the freezing cold – further confirmed by the anxious and frightened response to her short clearing of the throat to get her attention, whereupon she jumped some distance and drew her staff, shaking.
</p>

<p>“Earthmother shine upon you,” the gnome raised a steadying hand as she answered in Taur-ahe, much to her surprise, “I am not here to fight.”</p>

<p>Long and hard was the returned stare; both incredulous at the harsh Tauren tongue coming from the gentle intonations of the calm gnome, and at the claimed lack of wish to fight. After a moment though, she lowered her staff and cautiously replied, seemingly comfortable to lapse into her native language.
</p>

<p>“What do you want?”</p>

<p>“What do I want…” Miormi murmured with a smile, beginning to pace through the snow, hand making a broad, sweeping gesture, “Irrelevant, for it is your thoughts I ponder.”</p>

<p>“I already gave you everything I knew,” the tauren narrowed her eyes, becoming ever so slightly more guarded with her eyes following the pacing midget, “I just want to be left in peace.”</p>

<p>“Do you?” came an immediate retort, “You are a creature of action. You would not have become a cultist otherwise.”</p>

<p>“I want that life behind me,” she snapped back, “You aren’t helping.”</p>

<p>“Am I not?” Miormi chimed softly in return, “You’re still here. You haven’t run back to Conquest Hold, but you haven’t done anything for the Hammer. I watched.”
</p>

<p>“It’s safer that way,” the Tauren explained shakily after a moment’s hesitation, “Just leave me be.”</p>

<p>“You wonder if you can atone. Whether the spirits, the elements, the earthmother can forgive,” the Bronze persisted despite the request to leave, “Whether you are as doomed as they say.”</p>

<p>“The Hammer ordered me to stay put and be ready to fight, that I wouldn’t be harmed as long as I kept working,” she stated firmly.</p>

<p>“Of course they did,” the gnome chuckled, slowly brushing her fingers through the snow, “Are you a slave?”</p>

<p>“I – of course not!” shouted the Tauren indignantly.</p>

<p>“Calm, friend,” she wagged a finger, “Avalanches… Now, if you are not a slave, why must you obey to live?” came the beginning of the queries, blue eyes burrowing searchingly into the shivering would-be cultist, “A free woman chooses. A slave obeys.”</p>

<p>“What are you trying to say?” she responded in a guarded tone.</p>

<p>“I am saying you can atone. The Earthmother is threatened and needs your help. You need not be beholden to anyone – not me, not the cultists. You are beholden only to yourself – to do what you know is right.”</p>

<p>“It’s not that easy!” she protested vehemently, throwing up her hands and tossing her staff away in the process, “I can’t feel the elements anymore, it’s like they… Like they don’t want to hear me.”</p>

<p>“Then continue as you are,” she shrugged her shoulders with a dismissive wave of her hand, “When the Hammer tumbles down around you and your inaction has painted blood on your hand, none will be sympathetic,” Miormi turned to walk away in a very slow motion, “You have a choice, and a choice is all I offered.”</p>

<p>Just like that, in a moment, she’d vanished. Scurrying to the edge of the rocky outcrop, the Tauren couldn’t see where the gnome had gone and she sighed heavily, picking up her staff and using it as a walking aid to drag herself into the tent. Therein a pile of old communiques and a magical encoding device lay, the latter of which spun sedately on its axis with a soft wispy sound as the shackled elemental air within stirred in its confines.</p>

<p>She stared down toward the table, the words still echoing in her head as she tossed and turned between the choices she could make.
</p>

<p><center><b>A free woman chooses… A slave obeys.</b></center></p>