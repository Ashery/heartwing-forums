<div class="gif-container aerendragon">
	<img src="images/Aeren-avatar-dragon.gif" alt="Aeren"/>
</div>

<h3><center>Part Two</center></h3>
<p>"Father." Aeren said, bowing his head in respect. After that formality had ended, he showed his smile, a child would give to his own father. A proud and sincere smile. </p>

<p>Aeren rose to his feet, and the Father of Dragons opened his arms. As the father and son embraced each other. The father's words had a tone of compassion, and even kindness. "I am glad you are safe, my son." </p>

<p>Their arms parted from each other. Aeren was looking at his own father, as he replied. "Nothing I couldn't handle. Although I am sure, next time, it won't be that easy. I am glad to see you on your feet again. Some.. some thought that you could not survive that." Aeren said, hesitating at the end of his sentence, even as much faith he had in his father, Asherystrasz did not have the ability to defy a death sentence. But luckily, it never came to that. </p>

<p>"This war has taken a lot from us. But our lives have no meaning if's not to defend the world that allows us to live on it." The father looked downwards at the isle, while he spoke to his son. </p>

<p>The father shook his head. </p>

<p>"How is your consort, Aryiastrasza?" the father smiled at him more kind than before. He looked happy for his own son in that moment. </p>

<p>Aeren paused for a moment, and looked away in a pondering fashion. “The lady is doing well, my father. She seems to give a good impression on the recently born children of Heartwing. But.. let me ask you something. Did you bring me into Heartwing because of Aryiastrasza?” </p>

<p>Asherystrasz shook his head, streaming golden energy lines of magic escaped his eye sockets by the shaking movement of his head. </p>

<p>“I brought you to Heartwing because foremost I had been hiding you from the world of Azeroth for too long. I thought I could keep you safe behind closed doors, but I always knew the day you asked to see the world, I would give it to you. But you never defied such, you fancied reading books. I am the leader of Heartwing, but I am not able to be the person Heartwing is fighting with side by side on the battlefield, Hydra was the one who truly inspired the hearts of the dragons. Heartwing needs that. Heartwing needs you to do what Hydra did, and I knew there was nobody more capable than you. The young ones gets intimidated by those of veteran experience, those hardened by war. Those young never saw who the veterans were before. I truly believe, that when the people of Heartwing sees your development, they will also be inspired to develop themselves. Like a flower in a garden, seeing one arise, and a thousand will rise with it. You are that spark of life, my son”. </p>

<p>Aeren stretched his fingers, examining them with a ponderous look. He was astonished by the words of his father, and he hoped he wouldn’t disappoint him. His face expression turned from a surprised to a firm, as he gave a nod to his father. </p>

<p>The Father continued. </p>

<p>“Our spouses are what enhances us. They are the ones who inspire us, when we’ve fallen into the deepest abyss. I am your father, but whom you choose to call consort is your own to make. I did not choose before hand Aryiastrasza to be yours, that was the path you forged yourself. You are the only one who can choose who enhances and inspires you.” Asherystrasz turned to catch the eyes of his son, as he smiled finishing his sentence. </p>

<p>Aeren nodded his head. Pausing as he took a deep breath of relief. “Thank you, Father. You have shown great kindness.” </p>

<p>Asherystrasz turned his head, and broke the eye contact with his son. As his face expression turned grim but firm. </p>

<p>The Father brushed off the dirts of his elegant and majestic robe, as he rose to his feet. “Stay safe, my son. Everything may depend on it.” he said, as the Father’s cape began emitting magical energies, as a huge magical red cloud enveloped his body. As the cloud dimmed to nothing, a bird of elegant crimson burning feathers emerged from it. In a moment, and a few flaps of his wings, the Father had flown, almost surged away in the velocity of his avian form. </p>


<p>Aeren looked up at the sky. One of the Father’s burning feathers had fallen down gracefully before his feet. Before he could pick it up, he was interupted when he heard footsteps coming near him. Aeren turned around to see it was a paragon. Paragon Gaerilasgos. </p>

<p>“My lord, it is time to leave.” </p>

<p>Aeren nodded a single time with his head. </p>



<p>.. Several days later, near the Hinterlands. </p>


<p>Thunder emerged, following by the cavalry of the rain. Aeren’s face was in distress. He was in laying in the bed of the outpost he was currently investigating. He opened his eyes.. But something was amiss. Aeren tried to move, but his limbs were not reacting. He could look away, but his.. body was paralyzed by something. Aeren tried to use his magical senses, to see if magic was causing this. </p>

<p>But this was not magic. </p>

<p>The moonlight reflected on something sharp. Fangs. Long teeth, almost of a snake’s. But this was no mere snake, Aeren realized. Behind the intimidating teeth that was opening slowly, and ever getting closer to him, he saw dark red-orange eyes. The eyes of a snake, but he could see its figure was that of a humanoid. It had a long prehensile tail swirling around behind its visage. Aeren was not even able to his horrified face expression, he was only able to move his eyes </p>

<p>The snake creature was closing in on him. Like a spider who had put a fly into its paralyzing web, and he was mere moments from being swallowed. As it came further, the moonlight reflected again on it, revealing more of its teeth. Some mercury substance was dripping from its long twisting teeth. </p>

<p><b>Aeren was going to die.</b></p>