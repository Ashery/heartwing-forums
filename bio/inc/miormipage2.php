<div id="gif-container" class="miormi-container"><img src="/bio/images/miormi.gif" alt="Miormi"/></div>
<h3> Service II - 1 / 2 </h3>
<p>“Mio?”<p>

<p>The questioning intonation of Xaeridormi’s voice brought her out of one of many thoughtful trances, and she turned her draconic head slowly to her other side to peer at her; every time, there was a short spurt of thoughts. This young drake was liable to be the closest she’d have to a daughter to teach. An orphan. A needs-must. Despite this, there would always follow a curious emotional response, an attachment that she had already yielded to having a lack of understanding for. A tired, crooked smile formed on the dragon’s maw, warmed by her adoptive child’s inquiring mind.</p>

<p>“Xae,” she murmured, though in her form it came across as more of a low rumble as the elven-formed youngling was rather close by, “What seems to be the issue?”</p>

<p>“I have a question,” came the oft-head answer, “How does this lair have any air in it?”</p>

<p>“I believe there is an intake – ventilation – connected to somewhere on dry land.”</p>

<p>“Where does it connect to?” she pressed, almost on her tip-toes in curiosity despite the seemingly mundane nature of the question.</p>

<p>“A concealed antechamber – why do you ask, little Xae? I must admit I did not deign to investigate, only check that it was not going to be found, or be dangerous.”</p>

<p>“I want to go look! If the pipe is big enough.”</p>

<p>In truth, she was tired – greatly so, it had been a taxing few days back-and-forth between the Caverns and Northrend. Still no further word or peep from the Tauren. Scouting reports, skirmishes with the enemy, unexplained phenomena causing problems around Coldarra. How dearly she wanted to meditate for a while, basking in the warmth and scent of her burning incenses. Yet, here she was, pushing herself up onto all fours and stretching out her wings. Sating the curiosity of the young is a matter of Service to one’s flight, after all. Again, it gave her pause as she thought through this all while feeling a compulsion for affection. One more quandary; was it a matter of service, or a matter of maternal instinct? Were these two, specifically, necessarily different?</p>

<p>“Thank you, Mio!” Xae chirped to rouse her from the thought, interpreting her rising correctly as an affirmative, “I’ll go get ready!”</p>

<p>‘Get ready?’ the older dragon thought to herself, ‘What is she ...?’</p>

<p>In a very shortly lived vortex of light and sand she shrank into her gnomish form once more, reaching to grasp the enchanted silvery mace resting before her titanic shrine. In turn, she bound the various pieces of ornate crystalline armour to their place. Then arrived Xaeridormi, resplendent in battered travelling leathers, a wide-brimmed hat and a backpack. Plus something else.</p>

<p>“Xaeridormi,” she asked with a note of incredulity, “Is that a pistol?”</p>

<p>“Uh-huh,” she responded, “I was studying mortal archaeologists, and one of the Explorers League leaders…”</p>

<p>“Brann Bronzebeard,” she interrupted and lifted a hand to signify she understood, “You know we won’t have time for a full excavation here just yet, yes?”</p>

<p>“Yes,” Xae admitted glumly, “But you’re always telling me to read up – besides. You also say our talents are to use sparingly, not as a crutch. So I’m bringing conventional means to do things! Rope, brushes, torches!”
</p>

<p>‘I have taught this one too well for my own good.’ Miormi thought to herself, mildly amused.</p>

<p>“I am ill-placed to contradict without hypocrisy, so we shall move right on,” she said with a helpless smile. Something in the drake’s attempted aping of mortal archaeologists was just outright adorable.</p>

<p>In true titanic style, the connecting tunnel was gigantic and by its very nature accompanied by a strong breeze. For Miormi and her crystal armour – not such an issue – but for Xaeridormi’s Brann-styled hat, it rather frequently threatened to blow it from her head; a phenomenon not helped by the fact the sharply upward-rising ears of her blood elven disguise loosening the hat’s grip on her head to start with. Despite this silliness, she remained curious and attentive, examining the carvings and glowing lights along the length of the ventilation tunnel intently. From time to time she’d ask for a pause to lift a magnifying glass up to examine something. At its top ran a number of pulsating tubes that attracted her attention considerably as well, and yet not a question escaped her.</p>

<p>‘I suspect she wants to figure out the answers – or find them – herself… Interesting. Good…’ Miormi thought with a smile and a tinge of pride, occasionally even going so far as to wonder what those answers might be herself. Where did the sand come from? What are the pipes coming from? Are the scarabs decorative, or do they do something? Is it just ventilation and power supply?</p>

<p>“Isn’t this exciting?” Xaeri uttered quietly to her now-diminutive guardian, “Real, pulsing and breathing Titan stuff!”</p>

<p>“It has its charm,” she admitted cautiously, trying not to stoke her ward’s enthusiasm to the point of madness, “Just be careful. Old and quiet does not mean safe and harmless.”
</p>

<p>Drawing further into the tunnel, the entrance from which they had come became evermore a distant light, but they drew closer to another that signified the opening of the tunnel. Miormi’s recollections were faded from the last time she had checked, leaving the experience somewhat interesting even to her though she dared not show it, lest she fall prey to the infectious nature of Xaeridormi’s cheery curiosity. In many ways, the fact that the breeze and her ward’s footsteps were the only sounds other than her own to be heard made the journey more relaxing than she might have once given the hike credit for; so much so that she more or less lost track of how long they had wandered until reaching the far end where the tube opened up into a much larger chamber. It was not dissimilar from that which she had made her lair, but she had not picked up on as many of the variances and details as she now did examining it more keenly. The floor was mostly glass and spiralling, moving machines went down for what looked like miles, their function or purpose as elusive as they had ever been. Great groans of metal against metal echoed around the chamber and in the centre stood a large spherical contraption. The dull bronze colouring of the external plates betrayed little of this monument’s purpose, save for a few choice apertures through which the twisting and turning of intricate clockwork-like gears could be espied.
</p>

<p>It was this feature that drew the pair’s attention at first, and Xaeri needed a gentle coaxing to calm down just to prevent her from nestling her head in the fast-moving parts of the machine to get a closer look.</p>

<p>“It’s more intricate than normal,” the drake observed, “Titan machinery is so often huge and unwieldy, but these gears could have been made by gnomes…”</p>

<p>“Astute,” Miormi nodded in thought, glancing around the chamber from time to time as she recalled her need to stand sentinel, “Difficult to discern a purpose, regardless.”</p>

<p>“More pieces of the puzzle!” Xaeri proclaimed, moving away from the clockwork sphere to investigate the rest of the room. Jogging on her littler legs to keep up with her ward’s rapid steps, Miormi pursued as she homed in on another curious intricate structure – placed at regular intervals around this sphere were strange contraptions composed of crystalline lenses. Yet, no light did they focus – at that, there was no real source of bright light in the room to functionally use such lenses – and their arrangement was too random to have visible purpose. These were the thoughts she left Xaeri to deal with, however, as she turned her gaze on the rest of the chamber, wary of what their trespass may trigger or summon.</p>

<p>In this moment of looking elsewhere, the sound of a wheel being turned – squeaking like a badly oiled valve – erupted from behind her. Xaeri was twisting one lens – apparently built to rotate on its axis – and the others were rearranging. As the circular components moved and rotated, it became clear that some were not lenses, but mirrors.</p>

<p>“Xae,” she lived a placating hand, “We should not hasten to tamper until we know what we are tampering with.”</p>

<p>“But it’s nearly impossible, nobody knows how titanic machinery really works... Wait! Mio! How did you say you found your lair?”</p>

<p>The emerald eyes of her companion had almost set alight as she settled onto a train of thought, prompting Miormi to answer most cautiously indeed, “… By accident, while following a timeway.”</p>

<p>“What if this is… You know. Time-related?”</p>

<p>“All the more reason to treat it with caution, my young friend,” she warned again. “I am not an elder wyrm with the wisdom of the ages to manipulate time as the Titans did.”</p>

<p>“You’re too modest!” protested the drake, “I believe you can master it.”</p>

<p>With a weary sigh, Miormi closed her eyes and tilted her head back and cast out her otherwise dormant sense of timeflow. Normality reigned in such a way as to bring her great relief, although there were discrepancies in the chamber – minor faults, accelerations and decelerations in identical intensity and quantity. At a broad average, however, nothing was out of place – but as she had just discerned, nothing was out of place that could be physically observed.
</p>

<p>“Minor synchronisation faults,” she reported, “Minute distortions. The aftermath of a more tempestuous temporal event, if I had to render a guess.”</p>

<p>“Like a dragon trying to use timeways to teleport near it?” Xaeri teased openly, grinning broadly.</p>

<p>“… It’s a possibility,” she responded, giving the drake a searching squint, “But it wasn’t present where I arrived.”</p>

<p>“So you went through here where all the magic happened, and just sort of rolled out the other end!”</p>

<p>“A bit of a leap and a conclusion, isn’t it?” she countered, trying to smooth over the growing curiosity she recognised so well, “Document the room, and we can study it later.”</p>

<p>She received a squint in return, but Xaeri sighed her surrender and nodded, “Okay. Just documenting.”</p>

<p><center>***</center></p>

<p>That had been the theme of the evening. Miormi considered her a credit to archaeology in that she did document everything she saw in sickening detail, employing a rather eerie talent for rapid and accurate drawing to take elaborate records of the constructs and even some inscriptions; were it not for her constant attempts to fiddle and activate the machinery, she would have been the perfect student. Upon returning home, she had run straight to rest her weary head. She knew nobody was the perfect student, although she was familiar with the sensation she truly received – pride, and affection despite the fault. It had not, however, impeded her ability to caution and calm. That, then, was good.</p>

<p>It was not until waking some hour or so later that she would be introduced to one of the other facets of foster parenting for older children. Wanderlust, with a hint of rebellion. Not only was Miormi resting unexpectedly alone, she had a deep and profound explosion in her sense of the timeways. They were still orderly, but unmistakably moving and shifting. It was a sensation she occasionally bore while dreaming - her dreams were often related to her time senses, perhaps unsurprisingly, when they were not outright nightmares. The dragon’s eyes widened as she realised, however, that this was not a dream.</p>

<p>This is why you just shouldn't bother sleeping, Mio.</p>