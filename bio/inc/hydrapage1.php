<div id="gif-container" class="hydra-container"><img src="/bio/images/hydra-avatar.gif" alt="Hydra"/></div>
<h3> PART ONE </h3>
<p>“.. Hydra! Hydra! Hydra!” the crowd was shouting his name. Hydrastrasz looked down at his blade that was covered in blood. His blade no longer shone in his face, not at this moment. The blade blurred in his vision, as he focused on what was beneath him. 
</p>

<p>A black dragon wyrm. Hydra had aced the opposition against the black dragonflight and their allies in this battle. He was the one who managed to borrow his own blade deep into the neck of their leader, ending the battle and declaring its winner. Though Hydra truly knew, in war there is no winners.. Only sorrowful death. He looked to the crowd of dragons and mortals alike that had aided in the battle, they were waiting for him. Waiting for him to set fire to the fuse of taking glory from the battle. 
</p>

<p>Hydra put both of his hands at the hilt of his blade, as he bucked down in his knees. He channeled his strength and in a swift movement, he had pulled his blade free from the depths of the wyrm’s neck. A geyser of splattering blood surged from the neck wound, bathing Hydra’s torso with the tainted blood of the black dragonflight.</p>

<p>He looked at the crowd, as he heaved his blade into the air declaring the victory of the battle against Deathwing’s kin. </p>

<p>“Hydra! Hydra! Hydra!” the crowd bellowed in response.  </p>

<p><center> *** </center></p>

<p>Hydra slammed with his back onto the bed sheet. As he sighed deeply. “This.. war is not a victory march. How many did I lose..?” Hydra’s expression was anything but happy. “Velia, Niethra, Ceryla, Tyralilas, Walthan.. Please.. know you fought bravely, you fought with Heartwing. And I shall always remember your names, I will take care of your families, because if it wasn’t for you, I wouldn’t be here to enjoy my family.” Hydra swore to himself in his own thoughts. </p>

<p>Hydra rose his opened hand toward the ceiling. As his eyes began to get drowsy. “Your sacri-i-” </p>

<p>He closed the risen hand into a fist..</p>

<p>“It won’t be in vain.” </p>

<p><center> *** </center></p>

<p>Hydra sat on the ground in his human form. It was some kind of eden, life was everywhere. Not a single negative emotion or a single thing to defile it. It was a paradise truly. Hydra’s torso fell forward, as something tugged at his back and pushed him. “Daddy. Why you look so solemn?”. Hydra turned around and smiled at his own whelpling of a son. He patted his small draconic head. “I am not exactly sure. I think I am just happy.” he says to the young Galthrastrasz. 
</p>

<p>“You’re actually thinking? You know you should be careful about that, you might stress yourself out.” a playful voice spoke to him, followed by a heartily laughter. Hydra turned around with the young Galthrastrasz in his embrace, poking his nose in love of his own child. </p>

<p>The playful voice came from the mother of his children in her human form as well, the love of Hydra’s life, the only one he has ever truly loved. In her arms, another of their newborns.</p>

<p><b>Adiastrasza.</b></p>

<p>“Adia. You’re here too.” Hydra said with a surprised voice, but only widened the smile on his lips.</p>

<p>“Why wouldn’t I be? Our first clutch has just hatched. Now I wouldn’t be much of a broodmother, if I let you with all these pesky tricksters.” Adia said, while tenderly playing with the young whelpling in her embrace, giving the whelp the careful and playful love, only a mother would be able to give.</p>

<p>“Father!” came from a synchronized high pitch voice of a group saying the exact word simultaneously . Before Hydra could realize what was coming for him, he was tackled by a large group of whelps, pinning him down to the ground. One of the whelps that was straining him down spoke valiantly and proud. “See dad. You are nothing against our might! Maybe I should be the heir of Heartwing now, and not you!”. Hydra chuckled, letting the whelps pin him down. </p>

<p>“Alright, alright children. We don’t want a grumpy father complaining over how his back hurts again, would we?” Adia spoke while caressing the young infant in her arms lovely. </p>

<p>… Hydra reminisced back at what the valiant and proud whelp said..</p>

<p>“Heartwing...”</p>

<p>A negative sense of shocking epiphany filled Hydra’s body.</p>

<p>He looked around, as the whelps that was straining him was gone. The image of Adiastrasza and the whelp in her arms began to get smaller, as if they were slowly slipping away from his sight. Hydra desperately brought his hand forward, trying to reach out for the image of his beloved.</p>

<p>“Adia! Take my hand! Please.. take my hand..” he shouted out to the static face expression of Adiastrasza’s image. “Adia!”</p>

<p>The eden around him began to wither and crumble. The green paradise had turned to a grey and dark place, much like how the Dead Scar he saw in Quel’thalas had been. </p>

<p><center> *** </center></p>

<p>Hydra surged his torso upwards from the cover of his bedsheet. His body sweating heavily, he looking around desperately. Only to see the poorly lit room that was his. </p>

<p>A large noisily sound of shattering glass filled his room, as Hydra slammed his fist down in anger and had smashed the vase that was on the bed table next to him. The shard fragments of the vase digged into Hydra’s flesh that was his hand, his hand was dripping with blood. Hydra’s emotion of anger and frustration of weakness diminished, as he realized reality.. The bedsheet was smeared in blood.</p>

<p>Hydra took a deep breath. He regained his composure. “.. It was just a dream. Just a dream.”</p>

<p>Hydra shook his head from side to side, carefully removing the glass shards from his hands. He rose to his feet and opened his drawer. From it, he took a small bandage and wrapped it around his bloodied hand.</p>

<p><center> *** </center></p>

<p>Several days later. Hydra was called to a closed meeting in Heartwing. Giant stone doors opened in front of Hydra’s face. The architecture style of the room and the doors reminded him of Uldaman, the one time he visited it with Aeren. </p>

<p>To think, it all could have ended there. That he would never have been become of Heartwing or even met Adiastrasza if it wasn’t for Aeren saving him. Hydra was juvenile and reckless at that time, he rushed inside Uldaman thinking his strength could endure everything, and he would slash down any enemy that would defy the dragonflights. The enemies at that time had already prepared for intruders, and if it wasn’t for Aeren pushing Hydra away in the last moment, he would have fell directly into a death trap. Hydra was first frustrated at Aeren getting in his way, but after Aeren had thrown a large rock at the tile, a mechanism had activated and the tiles submerged and slided into hidden spots just beneath the ground, revealing a pit hidden below where human-sized spikes had impaled the unfortunate explorers. </p>

<p>Hydra entered the room. A large titanic round greenstone table was the center of the summit. Parallel to Hydra’s exact position, stood the leader of Heartwing, the large red leviathan that was Asherystrasz. Hydra walked up to the round stone table. Nodding his head in respect and acknowledgement to the other council members. </p>

<p>Asherystrasz addressed Hydra. “Glad you could join us, my son.” he glanced down at the table in front of Hydra, where Hydra had rested his hands on. “What happened to your hand..?”</p>

<p>Hydra answered immediatly. “.. A mere <b>accident,</b> father.”</p>