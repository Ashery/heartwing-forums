<div id="gif-container"><img src="images/Aeren-avatar.gif" alt="Aeren"/></div>
<h3> PART ONE </h3>
<p>Ash and embers were drifting from the sides of the Heartwing Tower. The flames, which Deathwing's blood had brought to Heartwing were ever-burning. Never ceasing. The Father had believed that the flames of corruption would only douse when the Destroyer had met his end. Only then will the ash stop clouding the sun.</p>

<p>Aerenstrasz tilted his head. He had wondered why such filthy flames hadn't affected the people close to them. Perhaps the Father did something to suppress the corruption? He did not know. Aeren rose himself to his feet and walked down to the lower floor. He did not enjoy the ash of the Destroyer's blood entering his lungs, even though they had no magical effect on his being. Aeren closed his eyes. He heard the birds sing. He took a deep breath and it filled his chest with air. Fresh air.</p>

<p>He opened his eyes, hearing commands being given out. It was the Paragons of Heartwing; his brother, Haelionstrasz, had returned and he was, for some reason, mad at a certain paragon. Although Haelion's responsibility didn't lie with the paragons but rather his own patrol, he always seemed to see himself responsible for everyone around him.</p>

<p>Aeren sighed and looked to the sky, shielding his eyes from the rays of the sun. He wondered, how he could be stronger, strong in mind and body like the Paragons were. But he also knew that the Paragons sacrificed a lot for that strength... A sacrifice he wasn't sure he could do. </p>

<p>‘For one to be Paragon. One must be reborn. One must be more than a dragon’. He remembered the speech the Dragon-General gave at the transition ceremony of the newly made Paragon Miormi. </p>

<p><center> *** </center></p>

<p>Dust from the ground began to swirl next to Aeren, sand manifesting from thin air, as it began to mold together and it formed a petite body with closed eyes. The sand structure changed saturation and it was visible, that it took on a gnomish appearance. The sand structure had become a gnome, a face Aeren was familiar with. The gnome opened his eyes.</p>

<p>"Xerasdormu.." Aeren said, looking at the gnome, who sat down next to him. He drifted sand from one palm to his other palm, lower down, emptying and filling another palm with sand as he repeated the process by raising the filled hand above the empty one. </p>

<p>"You still won't tell me who you are?" Aeren let out, sighing and deciding to keep his eyes on the view of the isle. </p>

<p>"I said I am a keeper of time. And an anomaly has centered itself around Heartwing. I intend to correct this anomaly." the gnome clapped his hands, proud and apparently excited over his own words. He didn't seem proud over himself... He seemed... excited over himself or something? </p>


<p>"You mean Millenius?" Aeren said, questioning the old bronze dragon. </p>

<p>"Normally, an Infinite influencing the past is a concern for the whole integrity of the Bronze Dragonflight. I don't know yet if this Infinite is a concern that is my responsibility. I heard rumors that should apparently have come from the great wyrms of the dragonflight. That very rarely in time, it is meant for the weaves of time to be influenced by other strings, not woven in the tapestry yet. As it stands, Millenius is not the primary concern for me. There's something more important than him brewing in the horizon."</p>

<p>"Deathwing?" the young dragon Aeren uttered, a name most dragons would tremble just mentioning. </p>

<p>"Deathwing is the general concern for all of us. However, to think that Deathwing is a single entity and the only enemy, we face what would be a great misinformation. Deathwing is the instrument of war. Not the war."</p>

<p>"Who are you, Xerasdormu?". </p>

<p>Xeras smiles at him and replies. "I have told you." He tilts his face backwards at the flames. "Good talk, heir of Heartwing". He says as his own body splits itself and his appearance drifts into the air, just like sand escaping in a desert. </p>

<p><center> *** </center></p>

<p>Aeren, as a magical being, could feel the presence of the greatest dragon he had ever met as he began to walk closer to him.</p>

<p>"How do you fare, my son?"</p>
