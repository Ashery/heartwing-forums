<?php $Content=  $_GET['Content']; // change the case # ?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Template Biography Page </title>
<link href="layout.css" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" href="images/wpicon.png" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
</head>

<body>
   <div id="wrapper">
      <header><img src="/styles/pbwow3/theme/images/theme/heartwinglogo.png" alt="Heartwing Logo" /></header>
      <nav>
      		<ul>
      			<a href="#" class="active aeren"><li>A Father's Son</li></a>
      			<a href="#"><li>Echoes of the Past</li></a>
      			<a href="#"><li>Menu Element 3 </li></a>
      		</ul>
      </nav>
      <main>
      	<div id="title-section">
      	<span class="story-type">Short Story: </span> </br> 
      	<h1>Echoes of the Past<h1>
      	<h2>Asherystrasz</h2>
      </div>
      <div id="text-wrapper-outer">
      	<div id="text-wrapper">
      		<div id="text-content">
<?php
switch ($Content)
{
  case one:
  include 'inc/aerenpage1.php';
  break;
  
  case two:
  include 'inc/aerenpage2.php';
  break;
  
  case three:
  echo 'inc/aerenpage3.php';
  break;
  default:
  include 'inc/aerenpage1.php';
  }
  ?>


<div class="pagination">
<ul>
<li><a href="index.php?Content=one">1</a></li>
<li><a href="index.php?Content=two">2</a></li>
<li></li>
</ul>
</div>
</div>
</div>
<div id="text-bottom"></div>
      	<div>
      </main>
      <footer><p>Site designed and developed by Jesper Pedersen</p></footer>

   </div>
</body>
</html>
