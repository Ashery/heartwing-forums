<?php $Content=  $_GET['Content']; // change the case # ?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Service</title>
<link href="/bio/layout.css" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" href="images/wpicon.png" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
</head>

<body class="miormi-page">

   <div id="wrapper">
   <?php 
   include '../inc/header.php'; 
    ?>
      <main class="miormi">
      	<div id="title-section">
      	<span class="story-type">Short Story: </span> </br> 
      	<h1>Service<h1>
      	<h2 class="miormi">Miormi</h2>
      </div>
      <div id="text-wrapper-outer">
      	<div id="text-wrapper">
      		<div id="text-content">
<?php
switch ($Content)
{
  case one:
  include '../inc/miormipage1.php';
  break;
  
  case two:
  include '../inc/miormipage2.php';
  break;
  
  case three:
  echo '../inc/miormipage3.php';
  break;
  default:
  include '../inc/miormipage1.php';
  }
  ?>


<div class="pagination">
<ul>
<li><a href="index.php?Content=one">1</a></li>
<li><a href="index.php?Content=two">2</a></li>
</ul>
</div>
</div>
</div>
<div id="text-bottom"></div>
      	<div>
      </main>
      <footer><p>“Clockwork Dreams” by NitroX72<br /> Original artwork by Blizzard Entertainment <br /> Site designed and developed by Jesper Pedersen</p></footer>

   </div>
</body>
</html>
