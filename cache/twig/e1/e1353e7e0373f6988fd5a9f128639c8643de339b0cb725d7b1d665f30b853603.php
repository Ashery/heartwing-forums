<?php

/* appform_body_mortal.php */
class __TwigTemplate_a9d7d27f8beffcd3a8e7140701fbf72e94e0500739db7937765e894d5a7cb7be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $location = "overall_header.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_header.html", "appform_body_mortal.php", 1)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 2
        echo "<div class=\"panel application-form\">
   <div class=\"inner\"><span class=\"corners-top\"><span></span></span>

   <div class=\"content\">
      
        <h2>Mortal Application</h2>
        <div class=\"change-template\">You can still apply as Drakonid by going to the forums <a href=\"http://heartwing.dk/viewforum.php?f=8\">here. </a>and follow the instructions there.
            <br />
            <br />
            <ul>
                <li class=\"disabled\"><a href=\"/application.php\"><a href=\"/application.php\"><img src=\"/images/application_icons/Dragon.png\" alt=\"Dragon Application\" /></a></li>
                <li><a href=\"/application_mortal.php\"><img src=\"/images/application_icons/Mortal.png\" alt=\"Mortal Application\" /></a></li>
                <li class=\"disabled\"><a href=\"/application_dragonspawn.php\"><img src=\"/images/application_icons/Dragonspawn.png\" alt=\"Dragonspawn Application\" /></a></li>
                <li class=\"disabled perma-disabled\"><a href=\"#\"><img src=\"/images/application_icons/Drakonid.png\" alt=\"Drakonid Application\" /><span>COMING SOON</span></a></li>
            </ul>

        </div>
        <span style=\"font-size: 1.7em; color: #b9ff9a; \">
            <div class=\"info rookiediscord\">
                Don't want your application to be posted in public? Got questions about backstory, character specialties? Questionable story? 
                Read our alternative recruitment post here: <a href=\"http://heartwing.dk/viewtopic.php?f=8&t=659\">Alternative Recruitment - Rookie Camp</a>
            </div>
        </span>
        <br>
        <br>

        <span style=\"font-size: 1.3em;\">
            <div class=\"info\">
            This is what we want you to include when sending us an application. It gives us an overview of your character and you as a person. Follow the template to the best of your ability. <br /><br />
            Disclaimer: <br />
            <ul>
            <li>We do not accept warlocks, death knights. Any classes related to the usage of fel or shadow</li>

            <li>We do not accept undead or demonic races. For example: Forsaken or demons</li>




            </ul>
            </div>
<img src=\"/images/mortal.jpg\" alt=\"Ysera\" />
<br /><br /><br />
        
        <form method=\"post\" action=\"";
        // line 45
        echo (isset($context["PROCESS_APPFORM"]) ? $context["PROCESS_APPFORM"] : null);
        echo "\" id=\"appform\">


    <div class=\"row\">
        <label>Image of character</label>
        <input type=\"url\" name=\"image\" placeholder=\"Use a direct link to an image\" required /><br />
    </div>

    <div class=\"row\">
        <label>Name</label>
        <input type=\"text\" name=\"name\" required /><br />
    </div>

    <div class=\"row\">
        <label>Race</label>
        <input type=\"text\" name=\"race\" required/> <br />
    </div>

    <div class=\"row\">
        <label>Gender</label>
        <select name=\"gender\" required>
                <option value=\"Male\">Male</option>
                <option value=\"Female\">Female</option>
        </select><br />
    </div> 

    <div class=\"row\">
        <label>Class</label>
        <input type=\"text\" name=\"class\" /><br />
    </div>

    <div class=\"row\">
        <label>Age</label>
        <input type=\"number\" id=\"age\" name=\"age\" required /><br />
    </div>

    <div class=\"row\">
        <label>Is your character a Dragonsworn? If so, to whom?</label>
        <input type=\"text\" name=\"dragonsworn\" required/> <br />
    </div>

    <div class=\"row\">
        <label>What dragonflight does your mortal feel most aligned with?</label>
        <select name=\"dfalignment\" required>
                <option value=\"None\" selected> -- No dragonflight in particular -- </option>
                <option value=\"Red Dragonflight\">Red Dragonflight</option>
                <option value=\"Green Dragonflight\">Green Dragonflight</option>
                <option value=\"Blue Dragonflight\">Blue Dragonflight</option>
                <option value=\"Bronze Dragonflight\">Bronze Dragonflight</option>
                <option value=\"Netherwing\">Netherwing</option>
        </select><br />
    </div>

    <div class=\"row\">
        <label>What is your character's physique type?:</label>
        <input type=\"text\" name=\"physique\" required/> <br />
    </div> 

    <div class=\"row\">
        <label>Your character's choice of magic:</label>
        <input type=\"text\" name=\"magicchoice\" required/> <br />
    </div> 

    <div class=\"row\">
        <label>Language(s)</label>
        <input type=\"text\" name=\"languages\" required/> <br />
    </div> 

    <div class=\"row\">
        <label>Describe your character's personality</label>
        <textarea name=\"characterpersonality\" required /></textarea><br />
    </div>    

    <div class=\"row\">
        <label>Describe your own personality</label>
        <textarea name=\"irlpersonality\" /></textarea><br />
    </div>

    <div class=\"row\">
        <label>What does your character interest himself/herself in? </label>
        <input type=\"text\" name=\"interests\" required/> <br />
    </div> 

    <div class=\"row\">
        <label>Minor spells/abilities of your character. Spells/abilities your character can easily cast/use: </label>
        <input type=\"text\" name=\"minorspells\" required/> <br />
    </div> 

    <div class=\"row\">
        <label>Most powerful spells/abilities of your character</label>
        <input type=\"text\" name=\"prestige\" required /><br />
    </div> 

    <div class=\"row\">
        <label>Summary of backstory</label>
        <textarea name=\"backstory\" required /></textarea>
        <div style=\"color: red;\">* Disclaimer: Please remember the higher the age you've entered. The more backstory we expect.</div>
        <br />
    </div>
 

    <div class=\"row\">
        <label>How much do your character know about dragons?</label>
        <textarea name=\"dragonknowic\" required /></textarea>
        <br />
    </div>

    <div class=\"row\">
        <label>How much do you know about dragon lore (you as a person)?</label>
        <textarea name=\"dragonknowooc\" required /></textarea>
        <br />
    </div>

    <div class=\"row\">
        <label>Your character's reason to join Heartwing:</label>
        <textarea name=\"ic-reason\" required /></textarea><br />
    </div>

    <div class=\"row\">
        <label>(OOC) What is your reason to join Heartwing:</label>
        <textarea name=\"ooc-reason\" /></textarea><br />
    </div>

    <div class=\"row\">
        <label>What other roleplay have you done before this character?</label>
        <textarea name=\"misc-rp\" /></textarea><br />
    </div>

     <div class=\"row checkbox-row\">
        <label>Are you aware of the guild's rules, the content of the Tome of Ancient Times forum and the lore of your dragonflight?</label>
        <input type=\"text\" name=\"understand\" required /><br />
    </div> 

     <div class=\"row checkbox-row\">
        <label>Have you understood Heartwing's current timeline?:</label>
        <input type=\"text\" name=\"timeline\" required /><br />
    </div> 

     <div class=\"row checkbox-row\">
        <label>Do you fully understand your dragonflight's lore, and other essential lore information:</label>
        <input type=\"text\" name=\"lore\" required /><br />
    </div> 


    <div class=\"row row-submit\">
        <input type=\"submit\" name=\"submit\" id =\"submit\" value=\"";
        // line 190
        echo $this->env->getExtension('phpbb')->lang("SUBMIT");
        echo "\" class=\"button1\" />
    </div>
       
        </span>
      
   </div>

   <span class=\"corners-bottom\"><span></span></span></div>
</div>
    

<script>

<!-- 



var formData = JSON.stringify(\$(\"#appform\").serializeArray());
console.log(JSON.stringify({ duration: item.duration, start: item.start }));
var matchingResults = JSON['data'].filter(function(x){ return x.id == 2; });

-->

</script>



";
        // line 217
        $location = "overall_footer.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_footer.html", "appform_body_mortal.php", 217)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        echo " ";
    }

    public function getTemplateName()
    {
        return "appform_body_mortal.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 217,  224 => 190,  76 => 45,  31 => 2,  19 => 1,);
    }
}
/* <!-- INCLUDE overall_header.html -->*/
/* <div class="panel application-form">*/
/*    <div class="inner"><span class="corners-top"><span></span></span>*/
/* */
/*    <div class="content">*/
/*       */
/*         <h2>Mortal Application</h2>*/
/*         <div class="change-template">You can still apply as Drakonid by going to the forums <a href="http://heartwing.dk/viewforum.php?f=8">here. </a>and follow the instructions there.*/
/*             <br />*/
/*             <br />*/
/*             <ul>*/
/*                 <li class="disabled"><a href="/application.php"><a href="/application.php"><img src="/images/application_icons/Dragon.png" alt="Dragon Application" /></a></li>*/
/*                 <li><a href="/application_mortal.php"><img src="/images/application_icons/Mortal.png" alt="Mortal Application" /></a></li>*/
/*                 <li class="disabled"><a href="/application_dragonspawn.php"><img src="/images/application_icons/Dragonspawn.png" alt="Dragonspawn Application" /></a></li>*/
/*                 <li class="disabled perma-disabled"><a href="#"><img src="/images/application_icons/Drakonid.png" alt="Drakonid Application" /><span>COMING SOON</span></a></li>*/
/*             </ul>*/
/* */
/*         </div>*/
/*         <span style="font-size: 1.7em; color: #b9ff9a; ">*/
/*             <div class="info rookiediscord">*/
/*                 Don't want your application to be posted in public? Got questions about backstory, character specialties? Questionable story? */
/*                 Read our alternative recruitment post here: <a href="http://heartwing.dk/viewtopic.php?f=8&t=659">Alternative Recruitment - Rookie Camp</a>*/
/*             </div>*/
/*         </span>*/
/*         <br>*/
/*         <br>*/
/* */
/*         <span style="font-size: 1.3em;">*/
/*             <div class="info">*/
/*             This is what we want you to include when sending us an application. It gives us an overview of your character and you as a person. Follow the template to the best of your ability. <br /><br />*/
/*             Disclaimer: <br />*/
/*             <ul>*/
/*             <li>We do not accept warlocks, death knights. Any classes related to the usage of fel or shadow</li>*/
/* */
/*             <li>We do not accept undead or demonic races. For example: Forsaken or demons</li>*/
/* */
/* */
/* */
/* */
/*             </ul>*/
/*             </div>*/
/* <img src="/images/mortal.jpg" alt="Ysera" />*/
/* <br /><br /><br />*/
/*         */
/*         <form method="post" action="{PROCESS_APPFORM}" id="appform">*/
/* */
/* */
/*     <div class="row">*/
/*         <label>Image of character</label>*/
/*         <input type="url" name="image" placeholder="Use a direct link to an image" required /><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Name</label>*/
/*         <input type="text" name="name" required /><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Race</label>*/
/*         <input type="text" name="race" required/> <br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Gender</label>*/
/*         <select name="gender" required>*/
/*                 <option value="Male">Male</option>*/
/*                 <option value="Female">Female</option>*/
/*         </select><br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Class</label>*/
/*         <input type="text" name="class" /><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Age</label>*/
/*         <input type="number" id="age" name="age" required /><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Is your character a Dragonsworn? If so, to whom?</label>*/
/*         <input type="text" name="dragonsworn" required/> <br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>What dragonflight does your mortal feel most aligned with?</label>*/
/*         <select name="dfalignment" required>*/
/*                 <option value="None" selected> -- No dragonflight in particular -- </option>*/
/*                 <option value="Red Dragonflight">Red Dragonflight</option>*/
/*                 <option value="Green Dragonflight">Green Dragonflight</option>*/
/*                 <option value="Blue Dragonflight">Blue Dragonflight</option>*/
/*                 <option value="Bronze Dragonflight">Bronze Dragonflight</option>*/
/*                 <option value="Netherwing">Netherwing</option>*/
/*         </select><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>What is your character's physique type?:</label>*/
/*         <input type="text" name="physique" required/> <br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Your character's choice of magic:</label>*/
/*         <input type="text" name="magicchoice" required/> <br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Language(s)</label>*/
/*         <input type="text" name="languages" required/> <br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Describe your character's personality</label>*/
/*         <textarea name="characterpersonality" required /></textarea><br />*/
/*     </div>    */
/* */
/*     <div class="row">*/
/*         <label>Describe your own personality</label>*/
/*         <textarea name="irlpersonality" /></textarea><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>What does your character interest himself/herself in? </label>*/
/*         <input type="text" name="interests" required/> <br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Minor spells/abilities of your character. Spells/abilities your character can easily cast/use: </label>*/
/*         <input type="text" name="minorspells" required/> <br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Most powerful spells/abilities of your character</label>*/
/*         <input type="text" name="prestige" required /><br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Summary of backstory</label>*/
/*         <textarea name="backstory" required /></textarea>*/
/*         <div style="color: red;">* Disclaimer: Please remember the higher the age you've entered. The more backstory we expect.</div>*/
/*         <br />*/
/*     </div>*/
/*  */
/* */
/*     <div class="row">*/
/*         <label>How much do your character know about dragons?</label>*/
/*         <textarea name="dragonknowic" required /></textarea>*/
/*         <br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>How much do you know about dragon lore (you as a person)?</label>*/
/*         <textarea name="dragonknowooc" required /></textarea>*/
/*         <br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Your character's reason to join Heartwing:</label>*/
/*         <textarea name="ic-reason" required /></textarea><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>(OOC) What is your reason to join Heartwing:</label>*/
/*         <textarea name="ooc-reason" /></textarea><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>What other roleplay have you done before this character?</label>*/
/*         <textarea name="misc-rp" /></textarea><br />*/
/*     </div>*/
/* */
/*      <div class="row checkbox-row">*/
/*         <label>Are you aware of the guild's rules, the content of the Tome of Ancient Times forum and the lore of your dragonflight?</label>*/
/*         <input type="text" name="understand" required /><br />*/
/*     </div> */
/* */
/*      <div class="row checkbox-row">*/
/*         <label>Have you understood Heartwing's current timeline?:</label>*/
/*         <input type="text" name="timeline" required /><br />*/
/*     </div> */
/* */
/*      <div class="row checkbox-row">*/
/*         <label>Do you fully understand your dragonflight's lore, and other essential lore information:</label>*/
/*         <input type="text" name="lore" required /><br />*/
/*     </div> */
/* */
/* */
/*     <div class="row row-submit">*/
/*         <input type="submit" name="submit" id ="submit" value="{L_SUBMIT}" class="button1" />*/
/*     </div>*/
/*        */
/*         </span>*/
/*       */
/*    </div>*/
/* */
/*    <span class="corners-bottom"><span></span></span></div>*/
/* </div>*/
/*     */
/* */
/* <script>*/
/* */
/* <!-- */
/* */
/* */
/* */
/* var formData = JSON.stringify($("#appform").serializeArray());*/
/* console.log(JSON.stringify({ duration: item.duration, start: item.start }));*/
/* var matchingResults = JSON['data'].filter(function(x){ return x.id == 2; });*/
/* */
/* -->*/
/* */
/* </script>*/
/* */
/* */
/* */
/* <!-- INCLUDE overall_footer.html --> */
