<?php

/* @paybas_rankpoststyling/event/memberlist_view_content_prepend.html */
class __TwigTemplate_7deafb772f68577b41e41e723f3d94f69e9fa9129d78d9eff5188dcf9386dfa9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div ";
        if ((isset($context["RANK_STYLE"]) ? $context["RANK_STYLE"] : null)) {
            echo " class=\"";
            echo (isset($context["RANK_STYLE"]) ? $context["RANK_STYLE"] : null);
            echo "\"";
        }
        echo ">
";
    }

    public function getTemplateName()
    {
        return "@paybas_rankpoststyling/event/memberlist_view_content_prepend.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div <!-- IF RANK_STYLE --> class="{RANK_STYLE}"<!-- ENDIF -->>*/
/* */
