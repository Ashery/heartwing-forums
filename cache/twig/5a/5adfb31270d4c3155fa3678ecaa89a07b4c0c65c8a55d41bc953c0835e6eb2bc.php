<?php

/* @paybas_gametooltips/event/overall_footer_after.html */
class __TwigTemplate_115d6067e6512670e682f86551045721e72aae54054ae8e8de4b28b275a9175b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ( !(isset($context["S_IS_BOT"]) ? $context["S_IS_BOT"] : null)) {
            // line 2
            echo "\t";
            if ((isset($context["GTTIPS_WOWHEAD"]) ? $context["GTTIPS_WOWHEAD"] : null)) {
                // line 3
                echo "\t\t<script src=\"//static.wowhead.com/widgets/power.js\"></script>
\t\t<script>var wowhead_tooltips = { \"colorlinks\": true, \"iconizelinks\": true, \"renamelinks\": true }</script>
\t";
            }
            // line 6
            echo "\t";
            if ((isset($context["GTTIPS_DIABLO3"]) ? $context["GTTIPS_DIABLO3"] : null)) {
                // line 7
                echo "\t\t<script src=\"//";
                echo (isset($context["GTTIPS_REGION"]) ? $context["GTTIPS_REGION"] : null);
                echo ".battle.net/d3/static/js/tooltips.js\"></script>
\t";
            }
            // line 9
            echo "\t";
            if ((isset($context["GTTIPS_WILDHEAP"]) ? $context["GTTIPS_WILDHEAP"] : null)) {
                // line 10
                echo "\t\t<script src=\"//static.wildheap.com/tooltip.js\"></script>";
                // line 11
                echo "\t\t<script>var WHTooltips = {\"colors\": true, \"icons\": true, \"rename\": true}</script>
\t";
            }
            // line 13
            echo "\t";
            if ((isset($context["GTTIPS_LOLTIP"]) ? $context["GTTIPS_LOLTIP"] : null)) {
                // line 14
                echo "\t\t<script src=\"http://www.arcana-gaming.com/Content/plugins/arcana-loltip/js\"></script>";
                // line 15
                echo "\t";
            }
            // line 16
            echo "\t";
            if ((isset($context["GTTIPS_XIVDB"]) ? $context["GTTIPS_XIVDB"] : null)) {
                // line 17
                echo "\t\t<script src=\"//xivdb.com/tooltips.js\"></script>
\t\t<script>var xivdb_tooltips = {'replaceName' : true,\t'colorName' : true,\t'showIcon' : true };</script>
\t";
            }
            // line 20
            echo "\t";
            if ((isset($context["GTTIPS_SWTOR"]) ? $context["GTTIPS_SWTOR"] : null)) {
                // line 21
                echo "\t\t<script src=\"http://static2.askmrrobot.com/swtor/tooltips.js\"></script>";
                // line 22
                echo "\t\t<script>var amr_options = { color: true, icon: true, iconSize: 18, name: true };</script>
\t";
            }
            // line 24
            echo "\t";
            if ((isset($context["GTTIPS_ZAM"]) ? $context["GTTIPS_ZAM"] : null)) {
                // line 25
                echo "\t\t<script>var zam_tooltips = { addIcons: true, colorLinks: true, renameLinks: true };</script>
\t\t<script src=\"//zam.zamimg.com/j/tooltips.js\"></script>";
                // line 27
                echo "\t";
            }
            // line 28
            echo "\t";
            if ((isset($context["GTTIPS_DESTINY"]) ? $context["GTTIPS_DESTINY"] : null)) {
                // line 29
                echo "\t\t<script>var zam_tooltips = { addIcons: true, colorLinks: true, renameLinks: true };</script>
\t\t<script src=\"//desjs.zamimg.com/asset/js/tooltips.min.js\"></script>
\t";
            }
            // line 32
            echo "\t";
            if ((isset($context["GTTIPS_ESOHEAD"]) ? $context["GTTIPS_ESOHEAD"] : null)) {
                // line 33
                echo "\t\t<script>var zam_tooltips = { addIcons: true, colorLinks: true, renameLinks: true };</script>
\t\t<script src=\"//eso.zamimg.com/asset/js/tooltips.min.js\"></script>";
                // line 35
                echo "\t";
            }
        }
    }

    public function getTemplateName()
    {
        return "@paybas_gametooltips/event/overall_footer_after.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 35,  92 => 33,  89 => 32,  84 => 29,  81 => 28,  78 => 27,  75 => 25,  72 => 24,  68 => 22,  66 => 21,  63 => 20,  58 => 17,  55 => 16,  52 => 15,  50 => 14,  47 => 13,  43 => 11,  41 => 10,  38 => 9,  32 => 7,  29 => 6,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* <!-- IF not S_IS_BOT -->*/
/* 	<!-- IF GTTIPS_WOWHEAD -->*/
/* 		<script src="//static.wowhead.com/widgets/power.js"></script>*/
/* 		<script>var wowhead_tooltips = { "colorlinks": true, "iconizelinks": true, "renamelinks": true }</script>*/
/* 	<!-- ENDIF -->*/
/* 	<!-- IF GTTIPS_DIABLO3 -->*/
/* 		<script src="//{GTTIPS_REGION}.battle.net/d3/static/js/tooltips.js"></script>*/
/* 	<!-- ENDIF -->*/
/* 	<!-- IF GTTIPS_WILDHEAP -->*/
/* 		<script src="//static.wildheap.com/tooltip.js"></script>{# dead? #}*/
/* 		<script>var WHTooltips = {"colors": true, "icons": true, "rename": true}</script>*/
/* 	<!-- ENDIF -->*/
/* 	<!-- IF GTTIPS_LOLTIP -->*/
/* 		<script src="http://www.arcana-gaming.com/Content/plugins/arcana-loltip/js"></script>{# doesn't support https #}*/
/* 	<!-- ENDIF -->*/
/* 	<!-- IF GTTIPS_XIVDB -->*/
/* 		<script src="//xivdb.com/tooltips.js"></script>*/
/* 		<script>var xivdb_tooltips = {'replaceName' : true,	'colorName' : true,	'showIcon' : true };</script>*/
/* 	<!-- ENDIF -->*/
/* 	<!-- IF GTTIPS_SWTOR -->*/
/* 		<script src="http://static2.askmrrobot.com/swtor/tooltips.js"></script>{# invalid https cert #}*/
/* 		<script>var amr_options = { color: true, icon: true, iconSize: 18, name: true };</script>*/
/* 	<!-- ENDIF -->*/
/* 	<!-- IF GTTIPS_ZAM -->*/
/* 		<script>var zam_tooltips = { addIcons: true, colorLinks: true, renameLinks: true };</script>*/
/* 		<script src="//zam.zamimg.com/j/tooltips.js"></script>{# doesn't support https apparently, even though the website says it does #}*/
/* 	<!-- ENDIF -->*/
/* 	<!-- IF GTTIPS_DESTINY -->*/
/* 		<script>var zam_tooltips = { addIcons: true, colorLinks: true, renameLinks: true };</script>*/
/* 		<script src="//desjs.zamimg.com/asset/js/tooltips.min.js"></script>*/
/* 	<!-- ENDIF -->*/
/* 	<!-- IF GTTIPS_ESOHEAD -->*/
/* 		<script>var zam_tooltips = { addIcons: true, colorLinks: true, renameLinks: true };</script>*/
/* 		<script src="//eso.zamimg.com/asset/js/tooltips.min.js"></script>{# invalid https cert but seems to work #}*/
/* 	<!-- ENDIF -->*/
/* <!-- ENDIF -->*/
/* */
