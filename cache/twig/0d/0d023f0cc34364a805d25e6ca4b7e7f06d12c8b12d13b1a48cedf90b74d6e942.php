<?php

/* @paybas_rankpoststyling/event/viewtopic_body_postrow_post_before.html */
class __TwigTemplate_1f67936cceb4044b99eef167e1b5e17308c7cdf9d3060606b65b3376359f8996 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"post-container";
        if ($this->getAttribute((isset($context["postrow"]) ? $context["postrow"] : null), "RANK_STYLE", array())) {
            echo " ";
            echo $this->getAttribute((isset($context["postrow"]) ? $context["postrow"] : null), "RANK_STYLE", array());
        }
        echo "\">
";
    }

    public function getTemplateName()
    {
        return "@paybas_rankpoststyling/event/viewtopic_body_postrow_post_before.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div class="post-container<!-- IF postrow.RANK_STYLE --> {postrow.RANK_STYLE}<!-- ENDIF -->">*/
/* */
