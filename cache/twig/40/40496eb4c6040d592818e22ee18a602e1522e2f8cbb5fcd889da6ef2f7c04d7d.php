<?php

/* overall_header.html */
class __TwigTemplate_62c84b12cdf610820291171e09327980e98b55ce250673b8b6ad4797cad080b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html dir=\"";
        // line 2
        echo (isset($context["S_CONTENT_DIRECTION"]) ? $context["S_CONTENT_DIRECTION"] : null);
        echo "\" lang=\"";
        echo (isset($context["S_USER_LANG"]) ? $context["S_USER_LANG"] : null);
        echo "\">

<head>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width\" /> ";
        // line 6
        echo (isset($context["META"]) ? $context["META"] : null);
        echo "
    <title>
        ";
        // line 8
        if ((isset($context["UNREAD_NOTIFICATIONS_COUNT"]) ? $context["UNREAD_NOTIFICATIONS_COUNT"] : null)) {
            echo "(";
            echo (isset($context["UNREAD_NOTIFICATIONS_COUNT"]) ? $context["UNREAD_NOTIFICATIONS_COUNT"] : null);
            echo ")
        ";
        }
        // line 10
        echo "        ";
        if (( !(isset($context["S_VIEWTOPIC"]) ? $context["S_VIEWTOPIC"] : null) &&  !(isset($context["S_VIEWFORUM"]) ? $context["S_VIEWFORUM"] : null))) {
            echo (isset($context["SITENAME"]) ? $context["SITENAME"] : null);
            echo " -
        ";
        }
        // line 12
        echo "        ";
        if ((isset($context["S_IN_MCP"]) ? $context["S_IN_MCP"] : null)) {
            echo $this->env->getExtension('phpbb')->lang("MCP");
            echo " -
        ";
        } elseif (        // line 13
(isset($context["S_IN_UCP"]) ? $context["S_IN_UCP"] : null)) {
            echo $this->env->getExtension('phpbb')->lang("UCP");
            echo " -
        ";
        }
        // line 14
        echo (isset($context["PAGE_TITLE"]) ? $context["PAGE_TITLE"] : null);
        echo "
        ";
        // line 15
        if (((isset($context["S_VIEWTOPIC"]) ? $context["S_VIEWTOPIC"] : null) || (isset($context["S_VIEWFORUM"]) ? $context["S_VIEWFORUM"] : null))) {
            echo "- ";
            echo (isset($context["SITENAME"]) ? $context["SITENAME"] : null);
            echo "
        ";
        }
        // line 17
        echo "    </title>

    ";
        // line 19
        if ((isset($context["S_ENABLE_FEEDS"]) ? $context["S_ENABLE_FEEDS"] : null)) {
            // line 20
            echo "    ";
            if ((isset($context["S_ENABLE_FEEDS_OVERALL"]) ? $context["S_ENABLE_FEEDS_OVERALL"] : null)) {
                // line 21
                echo "    <link rel=\"alternate\" type=\"application/atom+xml\" title=\"";
                echo $this->env->getExtension('phpbb')->lang("FEED");
                echo " - ";
                echo (isset($context["SITENAME"]) ? $context["SITENAME"] : null);
                echo "\" href=\"";
                echo (isset($context["U_FEED"]) ? $context["U_FEED"] : null);
                echo "\" />
    ";
            }
            // line 23
            echo "    ";
            if ((isset($context["S_ENABLE_FEEDS_NEWS"]) ? $context["S_ENABLE_FEEDS_NEWS"] : null)) {
                // line 24
                echo "    <link rel=\"alternate\" type=\"application/atom+xml\" title=\"";
                echo $this->env->getExtension('phpbb')->lang("FEED");
                echo " - ";
                echo $this->env->getExtension('phpbb')->lang("FEED_NEWS");
                echo "\" href=\"";
                echo (isset($context["U_FEED"]) ? $context["U_FEED"] : null);
                echo "?mode=news\" />
    ";
            }
            // line 26
            echo "    ";
            if ((isset($context["S_ENABLE_FEEDS_FORUMS"]) ? $context["S_ENABLE_FEEDS_FORUMS"] : null)) {
                // line 27
                echo "    <link rel=\"alternate\" type=\"application/atom+xml\" title=\"";
                echo $this->env->getExtension('phpbb')->lang("FEED");
                echo " - ";
                echo $this->env->getExtension('phpbb')->lang("ALL_FORUMS");
                echo "\" href=\"";
                echo (isset($context["U_FEED"]) ? $context["U_FEED"] : null);
                echo "?mode=forums\" />
    ";
            }
            // line 29
            echo "    ";
            if ((isset($context["S_ENABLE_FEEDS_TOPICS"]) ? $context["S_ENABLE_FEEDS_TOPICS"] : null)) {
                // line 30
                echo "    <link rel=\"alternate\" type=\"application/atom+xml\" title=\"";
                echo $this->env->getExtension('phpbb')->lang("FEED");
                echo " - ";
                echo $this->env->getExtension('phpbb')->lang("FEED_TOPICS_NEW");
                echo "\" href=\"";
                echo (isset($context["U_FEED"]) ? $context["U_FEED"] : null);
                echo "?mode=topics\" />
    ";
            }
            // line 32
            echo "    ";
            if ((isset($context["S_ENABLE_FEEDS_TOPICS_ACTIVE"]) ? $context["S_ENABLE_FEEDS_TOPICS_ACTIVE"] : null)) {
                // line 33
                echo "    <link rel=\"alternate\" type=\"application/atom+xml\" title=\"";
                echo $this->env->getExtension('phpbb')->lang("FEED");
                echo " - ";
                echo $this->env->getExtension('phpbb')->lang("FEED_TOPICS_ACTIVE");
                echo "\" href=\"";
                echo (isset($context["U_FEED"]) ? $context["U_FEED"] : null);
                echo "?mode=topics_active\" />
    ";
            }
            // line 35
            echo "    ";
            if (((isset($context["S_ENABLE_FEEDS_FORUM"]) ? $context["S_ENABLE_FEEDS_FORUM"] : null) && (isset($context["S_FORUM_ID"]) ? $context["S_FORUM_ID"] : null))) {
                // line 36
                echo "    <link rel=\"alternate\" type=\"application/atom+xml\" title=\"";
                echo $this->env->getExtension('phpbb')->lang("FEED");
                echo " - ";
                echo $this->env->getExtension('phpbb')->lang("FORUM");
                echo " - ";
                echo (isset($context["FORUM_NAME"]) ? $context["FORUM_NAME"] : null);
                echo "\" href=\"";
                echo (isset($context["U_FEED"]) ? $context["U_FEED"] : null);
                echo "?f=";
                echo (isset($context["S_FORUM_ID"]) ? $context["S_FORUM_ID"] : null);
                echo "\" />
    ";
            }
            // line 38
            echo "    ";
            if (((isset($context["S_ENABLE_FEEDS_TOPIC"]) ? $context["S_ENABLE_FEEDS_TOPIC"] : null) && (isset($context["S_TOPIC_ID"]) ? $context["S_TOPIC_ID"] : null))) {
                // line 39
                echo "    <link rel=\"alternate\" type=\"application/atom+xml\" title=\"";
                echo $this->env->getExtension('phpbb')->lang("FEED");
                echo " - ";
                echo $this->env->getExtension('phpbb')->lang("TOPIC");
                echo " - ";
                echo (isset($context["TOPIC_TITLE"]) ? $context["TOPIC_TITLE"] : null);
                echo "\" href=\"";
                echo (isset($context["U_FEED"]) ? $context["U_FEED"] : null);
                echo "?f=";
                echo (isset($context["S_FORUM_ID"]) ? $context["S_FORUM_ID"] : null);
                echo "&amp;t=";
                echo (isset($context["S_TOPIC_ID"]) ? $context["S_TOPIC_ID"] : null);
                echo "\" />
    ";
            }
            // line 41
            echo "    ";
        }
        // line 42
        echo "
    ";
        // line 43
        if ((isset($context["U_CANONICAL"]) ? $context["U_CANONICAL"] : null)) {
            // line 44
            echo "    <link rel=\"canonical\" href=\"";
            echo (isset($context["U_CANONICAL"]) ? $context["U_CANONICAL"] : null);
            echo "\" />
    ";
        }
        // line 46
        echo "
    <!--
\tphpBB style name: PBWoW 3
\tBased on style:   prosilver (this is the default phpBB3 style)
\tOriginal author:  Tom Beddard ( http://www.subBlue.com/ )
\tModified by: PayBas ( http://www.pbwow.com/ )
-->

    <link href=\"";
        // line 54
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/prosilver/theme/print.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"print\" title=\"printonly\" />
    ";
        // line 55
        if ((isset($context["S_ALLOW_CDN"]) ? $context["S_ALLOW_CDN"] : null)) {
            // line 56
            echo "    <link href=\"//fonts.googleapis.com/css?family=Open+Sans:600&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese\" rel=\"stylesheet\" />
    ";
        }
        // line 58
        echo "    <link href=\"https://fonts.googleapis.com/css?family=Encode+Sans\" rel=\"stylesheet\">



    <link href=\"";
        // line 62
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/prosilver/theme/common.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 63
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/prosilver/theme/links.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 64
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/prosilver/theme/content.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 65
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/prosilver/theme/buttons.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 66
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/prosilver/theme/cp.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 67
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/prosilver/theme/forms.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" />

    ";
        // line 69
        if (((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) !== "pbwow3")) {
            // line 70
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3/theme/stylesheet.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        }
        // line 72
        echo "    ";
        if (((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_wildstar")) {
            // line 73
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3_heroes/theme/stylesheet.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        } elseif ((        // line 74
(isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_xmas")) {
            // line 75
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3_wotlk/theme/stylesheet.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        }
        // line 77
        echo "
    <!-- CUSTOM FONT STYLESHEET -->
    <link href=\"https://fonts.googleapis.com/css?family=Titillium+Web\" rel=\"stylesheet\">
    <!-- <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'> -->
    <!-- <link href='https://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'> -->

    <link href=\"";
        // line 83
        echo (isset($context["T_STYLESHEET_LINK"]) ? $context["T_STYLESHEET_LINK"] : null);
        echo "\" rel=\"stylesheet\" />

    ";
        // line 85
        if ((((((((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3") || ((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_battlecry")) || ((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_garrison")) || ((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_pandaria")) || ((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_tbc")) || ((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_warlords"))) {
            // line 86
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3/theme/";
            echo (isset($context["T_THEME_LANG_NAME"]) ? $context["T_THEME_LANG_NAME"] : null);
            echo "/stylesheet.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        } elseif (((        // line 87
(isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_wotlk") || ((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_xmas"))) {
            // line 88
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3_wotlk/theme/";
            echo (isset($context["T_THEME_LANG_NAME"]) ? $context["T_THEME_LANG_NAME"] : null);
            echo "/stylesheet.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        }
        // line 90
        echo "
    <link href=\"";
        // line 91
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/prosilver/theme/responsive.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" media=\"only screen and (max-width: 700px), only screen and (max-device-width: 700px)\" />
    <link href=\"";
        // line 92
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/pbwow3/theme/responsive.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" media=\"only screen and (max-width: 700px), only screen and (max-device-width: 700px)\" />
    ";
        // line 93
        if ((((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_heroes") || ((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_wildstar"))) {
            // line 94
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3_heroes/theme/responsive.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" media=\"only screen and (max-width: 700px), only screen and (max-device-width: 700px)\" />
    ";
        }
        // line 96
        echo "
    ";
        // line 97
        if ((isset($context["S_PBWOW_AVATARS"]) ? $context["S_PBWOW_AVATARS"] : null)) {
            // line 98
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3/theme/game-icons.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        }
        // line 100
        echo "
    <link href=\"";
        // line 101
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/pbwow3/theme/custom.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" />
    ";
        // line 102
        if ((((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_heroes") || ((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_wildstar"))) {
            // line 103
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3_heroes/theme/custom.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        } elseif ((        // line 104
(isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_tech")) {
            // line 105
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3_tech/theme/custom.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        }
        // line 107
        echo "
    ";
        // line 108
        if (((isset($context["S_CONTENT_DIRECTION"]) ? $context["S_CONTENT_DIRECTION"] : null) == "rtl")) {
            // line 109
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/prosilver/theme/bidi.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    <link href=\"";
            // line 110
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3/theme/bidi.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        }
        // line 112
        echo "
    ";
        // line 113
        if ((isset($context["S_PLUPLOAD"]) ? $context["S_PLUPLOAD"] : null)) {
            // line 114
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/prosilver/theme/plupload.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        }
        // line 116
        echo "
    <!--[if lte IE 9]>
\t<link href=\"";
        // line 118
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/pbwow3/theme/tweaks.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" />
\t";
        // line 119
        if ((((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_heroes") || ((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_wildstar"))) {
            // line 120
            echo "    <link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3_heroes/theme/tweaks.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
    ";
        }
        // line 122
        echo "    <![endif]-->

";
        // line 124
        // line 125
        echo "
";
        // line 126
        echo $this->getAttribute((isset($context["definition"]) ? $context["definition"] : null), "STYLESHEETS", array());
        echo "

\t<link href=\"";
        // line 128
        echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
        echo "styles/pbwow3/theme/extensions.css?assets_version=";
        echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
        echo "\" rel=\"stylesheet\" />
\t";
        // line 129
        if ((((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_heroes") || ((isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_wildstar"))) {
            // line 130
            echo "\t\t<link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3_heroes/theme/extensions.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
\t";
        } elseif ((        // line 131
(isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null) == "pbwow3_tech")) {
            // line 132
            echo "\t\t<link href=\"";
            echo (isset($context["ROOT_PATH"]) ? $context["ROOT_PATH"] : null);
            echo "styles/pbwow3_tech/theme/extensions.css?assets_version=";
            echo (isset($context["T_ASSETS_VERSION"]) ? $context["T_ASSETS_VERSION"] : null);
            echo "\" rel=\"stylesheet\" />
\t";
        }
        // line 134
        echo "
";
        // line 135
        // line 136
        echo "</head>
<body id=\"phpbb\" class=\"nojs notouch section-";
        // line 137
        echo (isset($context["SCRIPT_NAME"]) ? $context["SCRIPT_NAME"] : null);
        echo " ";
        echo (isset($context["S_CONTENT_DIRECTION"]) ? $context["S_CONTENT_DIRECTION"] : null);
        echo " ";
        echo (isset($context["T_THEME_NAME"]) ? $context["T_THEME_NAME"] : null);
        echo " ";
        echo (isset($context["BODY_CLASS"]) ? $context["BODY_CLASS"] : null);
        echo "\">
";
        // line 138
        // line 139
        echo "
<div id=\"wrap\">
\t<a id=\"top\" class=\"anchor\" accesskey=\"t\"></a>
\t<div id=\"page-header\">
\t\t<div class=\"headerbar\">
\t\t\t<div class=\"inner\">

\t\t\t<div id=\"site-description\">
\t\t\t\t<a id=\"logo\" class=\"logo\" href=\"";
        // line 147
        if ((isset($context["U_SITE_HOME"]) ? $context["U_SITE_HOME"] : null)) {
            echo (isset($context["U_SITE_HOME"]) ? $context["U_SITE_HOME"] : null);
        } else {
            echo (isset($context["U_INDEX"]) ? $context["U_INDEX"] : null);
        }
        echo "\" title=\"";
        if ((isset($context["U_SITE_HOME"]) ? $context["U_SITE_HOME"] : null)) {
            echo $this->env->getExtension('phpbb')->lang("SITE_HOME");
        } else {
            echo $this->env->getExtension('phpbb')->lang("INDEX");
        }
        echo "\"></a>
\t\t\t\t<h1>";
        // line 148
        echo (isset($context["SITENAME"]) ? $context["SITENAME"] : null);
        echo "</h1>
\t\t\t\t<p>";
        // line 149
        echo (isset($context["SITE_DESCRIPTION"]) ? $context["SITE_DESCRIPTION"] : null);
        echo "</p>
\t\t\t\t<p class=\"skiplink\"><a href=\"#start_here\">";
        // line 150
        echo $this->env->getExtension('phpbb')->lang("SKIP");
        echo "</a></p>
\t\t\t\t<a href=\"";
        // line 151
        echo (isset($context["U_APPLICATION_FORM"]) ? $context["U_APPLICATION_FORM"] : null);
        echo "\" title=\"";
        echo $this->env->getExtension('phpbb')->lang("APPLICATION_FORM_EXPLAIN");
        echo "\" class=\"apply\">";
        echo $this->env->getExtension('phpbb')->lang("APPLICATION_FORM");
        echo "</a>
\t\t\t</div>

\t\t\t";
        // line 154
        if (((isset($context["S_DISPLAY_SEARCH"]) ? $context["S_DISPLAY_SEARCH"] : null) &&  !(isset($context["S_IN_SEARCH"]) ? $context["S_IN_SEARCH"] : null))) {
            // line 155
            echo "\t\t\t<div id=\"search-box\" class=\"search-box search-header\">
\t\t\t\t<form action=\"";
            // line 156
            echo (isset($context["U_SEARCH"]) ? $context["U_SEARCH"] : null);
            echo "\" method=\"get\" id=\"search\">
\t\t\t\t<fieldset>
\t\t\t\t\t<input name=\"keywords\" id=\"keywords\" type=\"search\" maxlength=\"128\" title=\"";
            // line 158
            echo $this->env->getExtension('phpbb')->lang("SEARCH_KEYWORDS");
            echo "\" class=\"inputbox search tiny\" size=\"20\" value=\"";
            echo (isset($context["SEARCH_WORDS"]) ? $context["SEARCH_WORDS"] : null);
            echo "\" placeholder=\"";
            echo $this->env->getExtension('phpbb')->lang("SEARCH_MINI");
            echo "\" />
\t\t\t\t\t<button class=\"button icon-button search-icon\" type=\"submit\" title=\"";
            // line 159
            echo $this->env->getExtension('phpbb')->lang("SEARCH");
            echo "\">";
            echo $this->env->getExtension('phpbb')->lang("SEARCH");
            echo "</button>
\t\t\t\t\t<a href=\"";
            // line 160
            echo (isset($context["U_SEARCH"]) ? $context["U_SEARCH"] : null);
            echo "\" class=\"button icon-button search-adv-icon\" title=\"";
            echo $this->env->getExtension('phpbb')->lang("SEARCH_ADV");
            echo "\">";
            echo $this->env->getExtension('phpbb')->lang("SEARCH_ADV");
            echo "</a>
\t\t\t\t\t";
            // line 161
            echo (isset($context["S_SEARCH_HIDDEN_FIELDS"]) ? $context["S_SEARCH_HIDDEN_FIELDS"] : null);
            echo "
\t\t\t\t</fieldset>
\t\t\t\t</form>
\t\t\t</div>
\t\t\t";
        }
        // line 166
        echo "
\t\t\t</div>
\t\t</div>

\t\t";
        // line 170
        $location = "navbar_header.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("navbar_header.html", "overall_header.html", 170)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 171
        echo "\t</div>

\t";
        // line 173
        // line 174
        echo "
\t<a id=\"start_here\" class=\"anchor\"></a>
\t<div id=\"page-body\">
\t\t";
        // line 177
        if ((((isset($context["S_BOARD_DISABLED"]) ? $context["S_BOARD_DISABLED"] : null) && (isset($context["S_USER_LOGGED_IN"]) ? $context["S_USER_LOGGED_IN"] : null)) && ((isset($context["U_MCP"]) ? $context["U_MCP"] : null) || (isset($context["U_ACP"]) ? $context["U_ACP"] : null)))) {
            // line 178
            echo "\t\t<div id=\"information\" class=\"rules\">
\t\t\t<div class=\"inner\">
\t\t\t\t<strong>";
            // line 180
            echo $this->env->getExtension('phpbb')->lang("INFORMATION");
            echo $this->env->getExtension('phpbb')->lang("COLON");
            echo "</strong> ";
            echo $this->env->getExtension('phpbb')->lang("BOARD_DISABLED");
            echo "
\t\t\t</div>
\t\t</div>
\t\t";
        }
        // line 184
        echo "
\t\t";
        // line 185
    }

    public function getTemplateName()
    {
        return "overall_header.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  632 => 185,  629 => 184,  619 => 180,  615 => 178,  613 => 177,  608 => 174,  607 => 173,  603 => 171,  591 => 170,  585 => 166,  577 => 161,  569 => 160,  563 => 159,  555 => 158,  550 => 156,  547 => 155,  545 => 154,  535 => 151,  531 => 150,  527 => 149,  523 => 148,  509 => 147,  499 => 139,  498 => 138,  488 => 137,  485 => 136,  484 => 135,  481 => 134,  473 => 132,  471 => 131,  464 => 130,  462 => 129,  456 => 128,  451 => 126,  448 => 125,  447 => 124,  443 => 122,  435 => 120,  433 => 119,  427 => 118,  423 => 116,  415 => 114,  413 => 113,  410 => 112,  403 => 110,  396 => 109,  394 => 108,  391 => 107,  383 => 105,  381 => 104,  374 => 103,  372 => 102,  366 => 101,  363 => 100,  355 => 98,  353 => 97,  350 => 96,  342 => 94,  340 => 93,  334 => 92,  328 => 91,  325 => 90,  315 => 88,  313 => 87,  304 => 86,  302 => 85,  297 => 83,  289 => 77,  281 => 75,  279 => 74,  272 => 73,  269 => 72,  261 => 70,  259 => 69,  252 => 67,  246 => 66,  240 => 65,  234 => 64,  228 => 63,  222 => 62,  216 => 58,  212 => 56,  210 => 55,  204 => 54,  194 => 46,  188 => 44,  186 => 43,  183 => 42,  180 => 41,  164 => 39,  161 => 38,  147 => 36,  144 => 35,  134 => 33,  131 => 32,  121 => 30,  118 => 29,  108 => 27,  105 => 26,  95 => 24,  92 => 23,  82 => 21,  79 => 20,  77 => 19,  73 => 17,  66 => 15,  62 => 14,  56 => 13,  50 => 12,  43 => 10,  36 => 8,  31 => 6,  22 => 2,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html dir="{S_CONTENT_DIRECTION}" lang="{S_USER_LANG}">*/
/* */
/* <head>*/
/*     <meta charset="utf-8">*/
/*     <meta name="viewport" content="width=device-width" /> {META}*/
/*     <title>*/
/*         <!-- IF UNREAD_NOTIFICATIONS_COUNT -->({UNREAD_NOTIFICATIONS_COUNT})*/
/*         <!-- ENDIF -->*/
/*         <!-- IF not S_VIEWTOPIC and not S_VIEWFORUM -->{SITENAME} -*/
/*         <!-- ENDIF -->*/
/*         <!-- IF S_IN_MCP -->{L_MCP} -*/
/*         <!-- ELSEIF S_IN_UCP -->{L_UCP} -*/
/*         <!-- ENDIF -->{PAGE_TITLE}*/
/*         <!-- IF S_VIEWTOPIC or S_VIEWFORUM -->- {SITENAME}*/
/*         <!-- ENDIF -->*/
/*     </title>*/
/* */
/*     <!-- IF S_ENABLE_FEEDS -->*/
/*     <!-- IF S_ENABLE_FEEDS_OVERALL -->*/
/*     <link rel="alternate" type="application/atom+xml" title="{L_FEED} - {SITENAME}" href="{U_FEED}" />*/
/*     <!-- ENDIF -->*/
/*     <!-- IF S_ENABLE_FEEDS_NEWS -->*/
/*     <link rel="alternate" type="application/atom+xml" title="{L_FEED} - {L_FEED_NEWS}" href="{U_FEED}?mode=news" />*/
/*     <!-- ENDIF -->*/
/*     <!-- IF S_ENABLE_FEEDS_FORUMS -->*/
/*     <link rel="alternate" type="application/atom+xml" title="{L_FEED} - {L_ALL_FORUMS}" href="{U_FEED}?mode=forums" />*/
/*     <!-- ENDIF -->*/
/*     <!-- IF S_ENABLE_FEEDS_TOPICS -->*/
/*     <link rel="alternate" type="application/atom+xml" title="{L_FEED} - {L_FEED_TOPICS_NEW}" href="{U_FEED}?mode=topics" />*/
/*     <!-- ENDIF -->*/
/*     <!-- IF S_ENABLE_FEEDS_TOPICS_ACTIVE -->*/
/*     <link rel="alternate" type="application/atom+xml" title="{L_FEED} - {L_FEED_TOPICS_ACTIVE}" href="{U_FEED}?mode=topics_active" />*/
/*     <!-- ENDIF -->*/
/*     <!-- IF S_ENABLE_FEEDS_FORUM and S_FORUM_ID -->*/
/*     <link rel="alternate" type="application/atom+xml" title="{L_FEED} - {L_FORUM} - {FORUM_NAME}" href="{U_FEED}?f={S_FORUM_ID}" />*/
/*     <!-- ENDIF -->*/
/*     <!-- IF S_ENABLE_FEEDS_TOPIC and S_TOPIC_ID -->*/
/*     <link rel="alternate" type="application/atom+xml" title="{L_FEED} - {L_TOPIC} - {TOPIC_TITLE}" href="{U_FEED}?f={S_FORUM_ID}&amp;t={S_TOPIC_ID}" />*/
/*     <!-- ENDIF -->*/
/*     <!-- ENDIF -->*/
/* */
/*     <!-- IF U_CANONICAL -->*/
/*     <link rel="canonical" href="{U_CANONICAL}" />*/
/*     <!-- ENDIF -->*/
/* */
/*     <!--*/
/* 	phpBB style name: PBWoW 3*/
/* 	Based on style:   prosilver (this is the default phpBB3 style)*/
/* 	Original author:  Tom Beddard ( http://www.subBlue.com/ )*/
/* 	Modified by: PayBas ( http://www.pbwow.com/ )*/
/* -->*/
/* */
/*     <link href="{ROOT_PATH}styles/prosilver/theme/print.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" type="text/css" media="print" title="printonly" />*/
/*     <!-- IF S_ALLOW_CDN -->*/
/*     <link href="//fonts.googleapis.com/css?family=Open+Sans:600&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese" rel="stylesheet" />*/
/*     <!-- ENDIF -->*/
/*     <link href="https://fonts.googleapis.com/css?family=Encode+Sans" rel="stylesheet">*/
/* */
/* */
/* */
/*     <link href="{ROOT_PATH}styles/prosilver/theme/common.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <link href="{ROOT_PATH}styles/prosilver/theme/links.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <link href="{ROOT_PATH}styles/prosilver/theme/content.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <link href="{ROOT_PATH}styles/prosilver/theme/buttons.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <link href="{ROOT_PATH}styles/prosilver/theme/cp.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <link href="{ROOT_PATH}styles/prosilver/theme/forms.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/* */
/*     <!-- IF T_THEME_NAME !== 'pbwow3' -->*/
/*     <link href="{ROOT_PATH}styles/pbwow3/theme/stylesheet.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ENDIF -->*/
/*     <!-- IF T_THEME_NAME == 'pbwow3_wildstar' -->*/
/*     <link href="{ROOT_PATH}styles/pbwow3_heroes/theme/stylesheet.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ELSEIF T_THEME_NAME == 'pbwow3_xmas' -->*/
/*     <link href="{ROOT_PATH}styles/pbwow3_wotlk/theme/stylesheet.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ENDIF -->*/
/* */
/*     <!-- CUSTOM FONT STYLESHEET -->*/
/*     <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">*/
/*     <!-- <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'> -->*/
/*     <!-- <link href='https://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css'> -->*/
/* */
/*     <link href="{T_STYLESHEET_LINK}" rel="stylesheet" />*/
/* */
/*     <!-- IF T_THEME_NAME == 'pbwow3' || T_THEME_NAME == 'pbwow3_battlecry' || T_THEME_NAME == 'pbwow3_garrison' || T_THEME_NAME == 'pbwow3_pandaria' || T_THEME_NAME == 'pbwow3_tbc' || T_THEME_NAME == 'pbwow3_warlords' -->*/
/*     <link href="{ROOT_PATH}styles/pbwow3/theme/{T_THEME_LANG_NAME}/stylesheet.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ELSEIF T_THEME_NAME == 'pbwow3_wotlk' || T_THEME_NAME == 'pbwow3_xmas' -->*/
/*     <link href="{ROOT_PATH}styles/pbwow3_wotlk/theme/{T_THEME_LANG_NAME}/stylesheet.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ENDIF -->*/
/* */
/*     <link href="{ROOT_PATH}styles/prosilver/theme/responsive.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" media="only screen and (max-width: 700px), only screen and (max-device-width: 700px)" />*/
/*     <link href="{ROOT_PATH}styles/pbwow3/theme/responsive.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" media="only screen and (max-width: 700px), only screen and (max-device-width: 700px)" />*/
/*     <!-- IF T_THEME_NAME == 'pbwow3_heroes' || T_THEME_NAME == 'pbwow3_wildstar' -->*/
/*     <link href="{ROOT_PATH}styles/pbwow3_heroes/theme/responsive.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" media="only screen and (max-width: 700px), only screen and (max-device-width: 700px)" />*/
/*     <!-- ENDIF -->*/
/* */
/*     <!-- IF S_PBWOW_AVATARS -->*/
/*     <link href="{ROOT_PATH}styles/pbwow3/theme/game-icons.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ENDIF -->*/
/* */
/*     <link href="{ROOT_PATH}styles/pbwow3/theme/custom.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- IF T_THEME_NAME == 'pbwow3_heroes' || T_THEME_NAME == 'pbwow3_wildstar' -->*/
/*     <link href="{ROOT_PATH}styles/pbwow3_heroes/theme/custom.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ELSEIF T_THEME_NAME == 'pbwow3_tech' -->*/
/*     <link href="{ROOT_PATH}styles/pbwow3_tech/theme/custom.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ENDIF -->*/
/* */
/*     <!-- IF S_CONTENT_DIRECTION eq 'rtl' -->*/
/*     <link href="{ROOT_PATH}styles/prosilver/theme/bidi.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <link href="{ROOT_PATH}styles/pbwow3/theme/bidi.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ENDIF -->*/
/* */
/*     <!-- IF S_PLUPLOAD -->*/
/*     <link href="{ROOT_PATH}styles/prosilver/theme/plupload.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ENDIF -->*/
/* */
/*     <!--[if lte IE 9]>*/
/* 	<link href="{ROOT_PATH}styles/pbwow3/theme/tweaks.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/* 	<!-- IF T_THEME_NAME == 'pbwow3_heroes' || T_THEME_NAME == 'pbwow3_wildstar' -->*/
/*     <link href="{ROOT_PATH}styles/pbwow3_heroes/theme/tweaks.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/*     <!-- ENDIF -->*/
/*     <![endif]-->*/
/* */
/* <!-- EVENT overall_header_head_append -->*/
/* */
/* {$STYLESHEETS}*/
/* */
/* 	<link href="{ROOT_PATH}styles/pbwow3/theme/extensions.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/* 	<!-- IF T_THEME_NAME == 'pbwow3_heroes' || T_THEME_NAME == 'pbwow3_wildstar' -->*/
/* 		<link href="{ROOT_PATH}styles/pbwow3_heroes/theme/extensions.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/* 	<!-- ELSEIF T_THEME_NAME == 'pbwow3_tech' -->*/
/* 		<link href="{ROOT_PATH}styles/pbwow3_tech/theme/extensions.css?assets_version={T_ASSETS_VERSION}" rel="stylesheet" />*/
/* 	<!-- ENDIF -->*/
/* */
/* <!-- EVENT overall_header_stylesheets_after -->*/
/* </head>*/
/* <body id="phpbb" class="nojs notouch section-{SCRIPT_NAME} {S_CONTENT_DIRECTION} {T_THEME_NAME} {BODY_CLASS}">*/
/* <!-- EVENT overall_header_body_before -->*/
/* */
/* <div id="wrap">*/
/* 	<a id="top" class="anchor" accesskey="t"></a>*/
/* 	<div id="page-header">*/
/* 		<div class="headerbar">*/
/* 			<div class="inner">*/
/* */
/* 			<div id="site-description">*/
/* 				<a id="logo" class="logo" href="<!-- IF U_SITE_HOME -->{U_SITE_HOME}<!-- ELSE -->{U_INDEX}<!-- ENDIF -->" title="<!-- IF U_SITE_HOME -->{L_SITE_HOME}<!-- ELSE -->{L_INDEX}<!-- ENDIF -->"></a>*/
/* 				<h1>{SITENAME}</h1>*/
/* 				<p>{SITE_DESCRIPTION}</p>*/
/* 				<p class="skiplink"><a href="#start_here">{L_SKIP}</a></p>*/
/* 				<a href="{U_APPLICATION_FORM}" title="{L_APPLICATION_FORM_EXPLAIN}" class="apply">{L_APPLICATION_FORM}</a>*/
/* 			</div>*/
/* */
/* 			<!-- IF S_DISPLAY_SEARCH and not S_IN_SEARCH -->*/
/* 			<div id="search-box" class="search-box search-header">*/
/* 				<form action="{U_SEARCH}" method="get" id="search">*/
/* 				<fieldset>*/
/* 					<input name="keywords" id="keywords" type="search" maxlength="128" title="{L_SEARCH_KEYWORDS}" class="inputbox search tiny" size="20" value="{SEARCH_WORDS}" placeholder="{L_SEARCH_MINI}" />*/
/* 					<button class="button icon-button search-icon" type="submit" title="{L_SEARCH}">{L_SEARCH}</button>*/
/* 					<a href="{U_SEARCH}" class="button icon-button search-adv-icon" title="{L_SEARCH_ADV}">{L_SEARCH_ADV}</a>*/
/* 					{S_SEARCH_HIDDEN_FIELDS}*/
/* 				</fieldset>*/
/* 				</form>*/
/* 			</div>*/
/* 			<!-- ENDIF -->*/
/* */
/* 			</div>*/
/* 		</div>*/
/* */
/* 		<!-- INCLUDE navbar_header.html -->*/
/* 	</div>*/
/* */
/* 	<!-- EVENT overall_header_page_body_before -->*/
/* */
/* 	<a id="start_here" class="anchor"></a>*/
/* 	<div id="page-body">*/
/* 		<!-- IF S_BOARD_DISABLED and S_USER_LOGGED_IN and (U_MCP or U_ACP) -->*/
/* 		<div id="information" class="rules">*/
/* 			<div class="inner">*/
/* 				<strong>{L_INFORMATION}{L_COLON}</strong> {L_BOARD_DISABLED}*/
/* 			</div>*/
/* 		</div>*/
/* 		<!-- ENDIF -->*/
/* */
/* 		<!-- EVENT overall_header_content_before -->*/
