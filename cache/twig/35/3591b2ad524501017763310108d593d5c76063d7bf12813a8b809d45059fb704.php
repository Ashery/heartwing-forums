<?php

/* appform_body_dragonspawn.php */
class __TwigTemplate_47383efa9823bb35ff471ba8d2083a5a01d6a3813e25e9397c8e40d136e71fdf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $location = "overall_header.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_header.html", "appform_body_dragonspawn.php", 1)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        // line 2
        echo "<script   src=\"https://code.jquery.com/jquery-2.2.3.min.js\"   integrity=\"sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=\"   crossorigin=\"anonymous\"></script>
<script>




jQuery (document).ready (function() {   
    jQuery('#df').unbind().on('change',function(){
        var system = jQuery(this).find('option:selected').val();
        var dfcolor = jQuery(\"#dfcolor\").val();
        var notice = jQuery(\"age-notice\"); 
        var age = jQuery(\"age\").val();

        if (system == \"Red Dragonflight\") { 
            color = \"#B22222\"; 
            console.log(jQuery(\"#dfcolor\").val); 
            jQuery(\"#dfcolor\").val(color);
            jQuery(\"#age-notice\").css(\"display\", \"block\");
        }
        else if (system == \"Blue Dragonflight\") {
            color = \"#00BFFF\"; 
            console.log(jQuery(\"#dfcolor\").val); 
            jQuery(\"#dfcolor\").val(color);
            jQuery(\"#age-notice\").css(\"display\", \"block\");
        }
        else if (system == \"Green Dragonflight\") {
            color = \"#90EE90\"; 
            console.log(jQuery(\"#dfcolor\").val); 
            jQuery(\"#dfcolor\").val(color);
            jQuery(\"#age-notice\").css(\"display\", \"block\");   
        }
        else if (system == \"Bronze Dragonflight\") {
            color = \"#DAA520\"; 
            console.log(jQuery(\"#dfcolor\").val); 
            jQuery(\"#dfcolor\").val(color);   
            jQuery(\"#age-notice\").css(\"display\", \"block\");
        }
        else if (system == \"Netherwing\") {
            color = \"#00CED1\"; 
            console.log(jQuery(\"#dfcolor\").val); 
            jQuery(\"#dfcolor\").val(color);    
            jQuery(\"#age-notice\").css(\"display\", \"block\");
        }



    });
});


</script>
<div class=\"panel application-form\">
   <div class=\"inner\"><span class=\"corners-top\"><span></span></span>

   <div class=\"content\">
      
        <h2>Dragonspawn Application</h2>
        <div class=\"change-template\">You can still apply as Drakonid by going to the forums <a href=\"http://heartwing.dk/viewforum.php?f=8\">here. </a>and follow the instructions there.
            <br />
            <br />
            <ul>
                <li class=\"disabled\"><a href=\"/application.php\"><a href=\"/application.php\"><img src=\"/images/application_icons/Dragon.png\" alt=\"Dragon Application\" /></a></li>
                <li class=\"disabled\"><a href=\"/application_mortal.php\"><img src=\"/images/application_icons/Mortal.png\" alt=\"Mortal Application\" /></a></li>
                <li><a href=\"#\"><img src=\"/images/application_icons/Dragonspawn.png\" alt=\"Dragonspawn Application\" /></a></li>
                <li class=\"disabled perma-disabled\"><a href=\"#\"><img src=\"/images/application_icons/Drakonid.png\" alt=\"Drakonid Application\" /><span>COMING SOON</span></a></li>
            </ul>

        </div>
        <span style=\"font-size: 1.7em; color: #b9ff9a; \">
            <div class=\"info rookiediscord\">
                Don't want your application to be posted in public? Got questions about backstory, character specialties? Questionable story? 
                Read our alternative recruitment post here: <a href=\"http://heartwing.dk/viewtopic.php?f=8&t=659\">Alternative Recruitment - Rookie Camp</a>
            </div>
        </span>
        <br>
        <br>

        <span style=\"font-size: 1.3em;\">
            <div class=\"info\">
            This is what we want you to include when sending us an application. It gives us an overview of your character and you as a person. Follow the template to the best of your ability. <br /><br />
            Disclaimer: <br />
            <ul>
            <li>We only accept Dragonspawn according to the five dragonflights; Red, Green, Blue, Bronze &amp; Netherwing</li>

            <li>We do not accept Dragonspawn wielders of fel, shadow or arts of death</li>




            </ul>
            </div>
<img src=\"/images/dragonspawn.jpg\" alt=\"Dragonspawn\" />
<br /><br /><br />
        
        <form method=\"post\" action=\"";
        // line 96
        echo (isset($context["PROCESS_APPFORM"]) ? $context["PROCESS_APPFORM"] : null);
        echo "\" id=\"appform\">



    <div class=\"row\">
        <label>Disguise Screenshot</label>
        <input type=\"url\" name=\"disguisescreenshot\" placeholder=\"Use a direct link to an image\" required /><br />
    </div>

    <div class=\"row\">
        <label>Drakonid Screenshot</label>
        <input type=\"url\" name=\"dragonspawnscreenshot\" placeholder=\"Use a direct link to an image\" required /><br />
    </div>

    <div class=\"row\">
        <label>Dragonflight</label>
        <select id=\"df\" name=\"dragonflight\" required>
                <option value=\"Red Dragonflight\">Red Dragonflight</option>
                <option value=\"Green Dragonflight\">Green Dragonflight</option>
                <option value=\"Blue Dragonflight\">Blue Dragonflight</option>
                <option value=\"Bronze Dragonflight\">Bronze Dragonflight</option>
                <option value=\"Netherwing\">Netherwing</option>
        </select><br />
        <input type=\"hidden\" value=\"red\" id=\"dfcolor\" name=\"dfcolor\"/>
    </div>

    <div class=\"row\">
        <label>";
        // line 123
        echo (isset($context["Optional"]) ? $context["Optional"] : null);
        echo " Pre-Dragonspawn Name</label>
        <input type=\"text\" name=\"predragonspawnname\" /><br />
    </div>

    <div class=\"row\">
        <label>Post-Dragonspawn Name</label>
        <input type=\"text\" name=\"postdragonspawnname\" required /> <br />
    </div>

    <div class=\"row\">
        <label>Gender</label>
        <select name=\"gender\" required>
                <option value=\"Male\">Male</option>
                <option value=\"Female\">Female</option>
        </select><br />
    </div> 

    <div class=\"row\">
        <label>Was your character born dragonspawn or later in life became one?: </label>
        <textarea name=\"isborndragonspawn\" /></textarea></ br>
    </div>

    <div class=\"row\">
        <label>Loyalty(-ies)</label>
        <input type=\"text\" name=\"loyalty\" required /><br />
    </div>

    <div class=\"row\">
        <label>Age</label>
        <input type=\"number\" id=\"age\" name=\"age\" required /><br />
    </div>

    <div class=\"row\">
        <label>Your character's view/opinion of Heartwing</label>
        <input type=\"text\" name=\"view\" required/> <br />
    </div>

    <div class=\"row\">
        <label>Who or which organizations is your character connected with?:</label>
        <input type=\"text\" name=\"physique\" required/> <br />
    </div> 

    <div class=\"row\">
        <label>Respect for authority:</label>
        <input type=\"text\" name=\"authority\" required/> <br />
    </div> 

    <div class=\"row\">
        <label>Describe your character's personality</label>
        <textarea name=\"characterpersonality\" required/></textarea><br />
    </div> 

    <div class=\"row\">
        <label>Describe your own personality:</label>
        <textarea name=\"irlpersonality\" required /></textarea><br />
    </div>

    <div class=\"row\">
        <label>What does your character interest himself/herself in? </label>
        <input type=\"text\" name=\"interests\" required/> <br />
    </div> 

    <div class=\"row\">
        <label>";
        // line 186
        echo (isset($context["Optional"]) ? $context["Optional"] : null);
        echo " Summary of backstory - before your character became a dragonspawn:</label>
        <textarea name=\"prebackstory\" /></textarea><br />
    </div>

    <div class=\"row\">
        <label>Summary of backstory as dragonspawn:</label>
        <textarea name=\"dragonspawnbackstory\" required /></textarea><br />
    </div>

    <div class=\"row\">
        <label>Minor spells/abilities of your character. Spells/abilities your character can easily cast/use: </label>
        <input type=\"text\" name=\"minorspells\" required/> <br />
    </div> 

    <div class=\"row\">
        <label>Most powerful spells/abilities of your character</label>
        <input type=\"text\" name=\"prestige\" required /><br />
    </div>

    <div class=\"row checkbox-row\">
        <label>Is this your first time at dragon/dragonspawn roleplay, or do you have experience?:</label>
        <input type=\"text\" name=\"experience\" required /><br />
    </div> 

    <div class=\"row checkbox-row\">
        <label>Have you fully read this guild thread, and are fully aware of the guild's rules and content?:</label>
        <input type=\"text\" name=\"understand\" required /><br />
    </div> 

     <div class=\"row  checkbox-row\">
        <label>Have you understood Heartwing's current timeline?:</label>
        <input type=\"text\" name=\"timeline\" required /><br />
    </div> 

     <div class=\"row checkbox-row\">
        <label>Do you fully understand your dragonflight's lore, and other essential lore information:</label>
        <input type=\"text\" name=\"lore\" required /><br />
    </div> 

    <div class=\"row\">
        <label>Your character's reason to join Heartwing:</label>
        <textarea name=\"ic-reason\" required /></textarea><br />
    </div>

    <div class=\"row\">
        <label>(OOC) What is your reason to join Heartwing:</label>
        <textarea name=\"ooc-reason\" /></textarea><br />
    </div>

    <div class=\"row\">
        <label>What other roleplay have you done before this character?</label>
        <textarea name=\"misc-rp\" /></textarea><br />
    </div>


    <div class=\"row row-submit\">
        <input type=\"submit\" name=\"submit\" id =\"submit\" value=\"";
        // line 242
        echo $this->env->getExtension('phpbb')->lang("SUBMIT");
        echo "\" class=\"button1\" />
    </div>
       
        </span>
      
   </div>

   <span class=\"corners-bottom\"><span></span></span></div>
</div>
    

<script>

<!-- 



var formData = JSON.stringify(\$(\"#appform\").serializeArray());
console.log(JSON.stringify({ duration: item.duration, start: item.start }));
var matchingResults = JSON['data'].filter(function(x){ return x.id == 2; });

-->

</script>



";
        // line 269
        $location = "overall_footer.html";
        $namespace = false;
        if (strpos($location, '@') === 0) {
            $namespace = substr($location, 1, strpos($location, '/') - 1);
            $previous_look_up_order = $this->env->getNamespaceLookUpOrder();
            $this->env->setNamespaceLookUpOrder(array($namespace, '__main__'));
        }
        $this->loadTemplate("overall_footer.html", "appform_body_dragonspawn.php", 269)->display($context);
        if ($namespace) {
            $this->env->setNamespaceLookUpOrder($previous_look_up_order);
        }
        echo " ";
    }

    public function getTemplateName()
    {
        return "appform_body_dragonspawn.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  312 => 269,  282 => 242,  223 => 186,  157 => 123,  127 => 96,  31 => 2,  19 => 1,);
    }
}
/* <!-- INCLUDE overall_header.html -->*/
/* <script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>*/
/* <script>*/
/* */
/* */
/* */
/* */
/* jQuery (document).ready (function() {   */
/*     jQuery('#df').unbind().on('change',function(){*/
/*         var system = jQuery(this).find('option:selected').val();*/
/*         var dfcolor = jQuery("#dfcolor").val();*/
/*         var notice = jQuery("age-notice"); */
/*         var age = jQuery("age").val();*/
/* */
/*         if (system == "Red Dragonflight") { */
/*             color = "#B22222"; */
/*             console.log(jQuery("#dfcolor").val); */
/*             jQuery("#dfcolor").val(color);*/
/*             jQuery("#age-notice").css("display", "block");*/
/*         }*/
/*         else if (system == "Blue Dragonflight") {*/
/*             color = "#00BFFF"; */
/*             console.log(jQuery("#dfcolor").val); */
/*             jQuery("#dfcolor").val(color);*/
/*             jQuery("#age-notice").css("display", "block");*/
/*         }*/
/*         else if (system == "Green Dragonflight") {*/
/*             color = "#90EE90"; */
/*             console.log(jQuery("#dfcolor").val); */
/*             jQuery("#dfcolor").val(color);*/
/*             jQuery("#age-notice").css("display", "block");   */
/*         }*/
/*         else if (system == "Bronze Dragonflight") {*/
/*             color = "#DAA520"; */
/*             console.log(jQuery("#dfcolor").val); */
/*             jQuery("#dfcolor").val(color);   */
/*             jQuery("#age-notice").css("display", "block");*/
/*         }*/
/*         else if (system == "Netherwing") {*/
/*             color = "#00CED1"; */
/*             console.log(jQuery("#dfcolor").val); */
/*             jQuery("#dfcolor").val(color);    */
/*             jQuery("#age-notice").css("display", "block");*/
/*         }*/
/* */
/* */
/* */
/*     });*/
/* });*/
/* */
/* */
/* </script>*/
/* <div class="panel application-form">*/
/*    <div class="inner"><span class="corners-top"><span></span></span>*/
/* */
/*    <div class="content">*/
/*       */
/*         <h2>Dragonspawn Application</h2>*/
/*         <div class="change-template">You can still apply as Drakonid by going to the forums <a href="http://heartwing.dk/viewforum.php?f=8">here. </a>and follow the instructions there.*/
/*             <br />*/
/*             <br />*/
/*             <ul>*/
/*                 <li class="disabled"><a href="/application.php"><a href="/application.php"><img src="/images/application_icons/Dragon.png" alt="Dragon Application" /></a></li>*/
/*                 <li class="disabled"><a href="/application_mortal.php"><img src="/images/application_icons/Mortal.png" alt="Mortal Application" /></a></li>*/
/*                 <li><a href="#"><img src="/images/application_icons/Dragonspawn.png" alt="Dragonspawn Application" /></a></li>*/
/*                 <li class="disabled perma-disabled"><a href="#"><img src="/images/application_icons/Drakonid.png" alt="Drakonid Application" /><span>COMING SOON</span></a></li>*/
/*             </ul>*/
/* */
/*         </div>*/
/*         <span style="font-size: 1.7em; color: #b9ff9a; ">*/
/*             <div class="info rookiediscord">*/
/*                 Don't want your application to be posted in public? Got questions about backstory, character specialties? Questionable story? */
/*                 Read our alternative recruitment post here: <a href="http://heartwing.dk/viewtopic.php?f=8&t=659">Alternative Recruitment - Rookie Camp</a>*/
/*             </div>*/
/*         </span>*/
/*         <br>*/
/*         <br>*/
/* */
/*         <span style="font-size: 1.3em;">*/
/*             <div class="info">*/
/*             This is what we want you to include when sending us an application. It gives us an overview of your character and you as a person. Follow the template to the best of your ability. <br /><br />*/
/*             Disclaimer: <br />*/
/*             <ul>*/
/*             <li>We only accept Dragonspawn according to the five dragonflights; Red, Green, Blue, Bronze &amp; Netherwing</li>*/
/* */
/*             <li>We do not accept Dragonspawn wielders of fel, shadow or arts of death</li>*/
/* */
/* */
/* */
/* */
/*             </ul>*/
/*             </div>*/
/* <img src="/images/dragonspawn.jpg" alt="Dragonspawn" />*/
/* <br /><br /><br />*/
/*         */
/*         <form method="post" action="{PROCESS_APPFORM}" id="appform">*/
/* */
/* */
/* */
/*     <div class="row">*/
/*         <label>Disguise Screenshot</label>*/
/*         <input type="url" name="disguisescreenshot" placeholder="Use a direct link to an image" required /><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Drakonid Screenshot</label>*/
/*         <input type="url" name="dragonspawnscreenshot" placeholder="Use a direct link to an image" required /><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Dragonflight</label>*/
/*         <select id="df" name="dragonflight" required>*/
/*                 <option value="Red Dragonflight">Red Dragonflight</option>*/
/*                 <option value="Green Dragonflight">Green Dragonflight</option>*/
/*                 <option value="Blue Dragonflight">Blue Dragonflight</option>*/
/*                 <option value="Bronze Dragonflight">Bronze Dragonflight</option>*/
/*                 <option value="Netherwing">Netherwing</option>*/
/*         </select><br />*/
/*         <input type="hidden" value="red" id="dfcolor" name="dfcolor"/>*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>{Optional} Pre-Dragonspawn Name</label>*/
/*         <input type="text" name="predragonspawnname" /><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Post-Dragonspawn Name</label>*/
/*         <input type="text" name="postdragonspawnname" required /> <br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Gender</label>*/
/*         <select name="gender" required>*/
/*                 <option value="Male">Male</option>*/
/*                 <option value="Female">Female</option>*/
/*         </select><br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Was your character born dragonspawn or later in life became one?: </label>*/
/*         <textarea name="isborndragonspawn" /></textarea></ br>*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Loyalty(-ies)</label>*/
/*         <input type="text" name="loyalty" required /><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Age</label>*/
/*         <input type="number" id="age" name="age" required /><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Your character's view/opinion of Heartwing</label>*/
/*         <input type="text" name="view" required/> <br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Who or which organizations is your character connected with?:</label>*/
/*         <input type="text" name="physique" required/> <br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Respect for authority:</label>*/
/*         <input type="text" name="authority" required/> <br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Describe your character's personality</label>*/
/*         <textarea name="characterpersonality" required/></textarea><br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Describe your own personality:</label>*/
/*         <textarea name="irlpersonality" required /></textarea><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>What does your character interest himself/herself in? </label>*/
/*         <input type="text" name="interests" required/> <br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>{Optional} Summary of backstory - before your character became a dragonspawn:</label>*/
/*         <textarea name="prebackstory" /></textarea><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Summary of backstory as dragonspawn:</label>*/
/*         <textarea name="dragonspawnbackstory" required /></textarea><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>Minor spells/abilities of your character. Spells/abilities your character can easily cast/use: </label>*/
/*         <input type="text" name="minorspells" required/> <br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Most powerful spells/abilities of your character</label>*/
/*         <input type="text" name="prestige" required /><br />*/
/*     </div>*/
/* */
/*     <div class="row checkbox-row">*/
/*         <label>Is this your first time at dragon/dragonspawn roleplay, or do you have experience?:</label>*/
/*         <input type="text" name="experience" required /><br />*/
/*     </div> */
/* */
/*     <div class="row checkbox-row">*/
/*         <label>Have you fully read this guild thread, and are fully aware of the guild's rules and content?:</label>*/
/*         <input type="text" name="understand" required /><br />*/
/*     </div> */
/* */
/*      <div class="row  checkbox-row">*/
/*         <label>Have you understood Heartwing's current timeline?:</label>*/
/*         <input type="text" name="timeline" required /><br />*/
/*     </div> */
/* */
/*      <div class="row checkbox-row">*/
/*         <label>Do you fully understand your dragonflight's lore, and other essential lore information:</label>*/
/*         <input type="text" name="lore" required /><br />*/
/*     </div> */
/* */
/*     <div class="row">*/
/*         <label>Your character's reason to join Heartwing:</label>*/
/*         <textarea name="ic-reason" required /></textarea><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>(OOC) What is your reason to join Heartwing:</label>*/
/*         <textarea name="ooc-reason" /></textarea><br />*/
/*     </div>*/
/* */
/*     <div class="row">*/
/*         <label>What other roleplay have you done before this character?</label>*/
/*         <textarea name="misc-rp" /></textarea><br />*/
/*     </div>*/
/* */
/* */
/*     <div class="row row-submit">*/
/*         <input type="submit" name="submit" id ="submit" value="{L_SUBMIT}" class="button1" />*/
/*     </div>*/
/*        */
/*         </span>*/
/*       */
/*    </div>*/
/* */
/*    <span class="corners-bottom"><span></span></span></div>*/
/* </div>*/
/*     */
/* */
/* <script>*/
/* */
/* <!-- */
/* */
/* */
/* */
/* var formData = JSON.stringify($("#appform").serializeArray());*/
/* console.log(JSON.stringify({ duration: item.duration, start: item.start }));*/
/* var matchingResults = JSON['data'].filter(function(x){ return x.id == 2; });*/
/* */
/* -->*/
/* */
/* </script>*/
/* */
/* */
/* */
/* <!-- INCLUDE overall_footer.html --> */
