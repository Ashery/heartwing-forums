<?php

/* @paybas_rankpoststyling/event/search_results_post_before.html */
class __TwigTemplate_4fd416022e84dcd5e625591d1f124ca13710f799693bc0413bbcee36e0337f3f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"post-container";
        if ($this->getAttribute((isset($context["searchresults"]) ? $context["searchresults"] : null), "RANK_STYLE", array())) {
            echo " ";
            echo $this->getAttribute((isset($context["searchresults"]) ? $context["searchresults"] : null), "RANK_STYLE", array());
        }
        echo "\">
";
    }

    public function getTemplateName()
    {
        return "@paybas_rankpoststyling/event/search_results_post_before.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div class="post-container<!-- IF searchresults.RANK_STYLE --> {searchresults.RANK_STYLE}<!-- ENDIF -->">*/
/* */
