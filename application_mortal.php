<?php
/**
* @package application.php
* @copyright (c) JimA http://beta-garden.com 2009
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

$application_form = 1;

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup('mods/application');

// You need to login before being able to send out an application
if ($user->data['user_id'] == ANONYMOUS)
{
    login_box('', $user->lang['LOGIN_APPLICATION_FORM']);
}

include($phpbb_root_path . 'includes/functions_posting.' . $phpEx);

// Let's set the configuration, this is the ID of the forum where the post goes to
$forumid_send = 2;
$submit = (isset($_POST['submit'])) ? true : false;

	if ($submit)
	{
		// Setting the variables we need to submit the post to the forum where all the applications come in
		$apply_subject  = '['.utf8_normalize_nfc(request_var('race', '', true)).'] '.' '.utf8_normalize_nfc(request_var('name', '', true));
		$apply_post     = 
		sprintf($user->lang['APPLICATION_MESSAGE_MORTAL'], 
		utf8_normalize_nfc(request_var('image', '', true)),
		utf8_normalize_nfc(request_var('name', '', true)),  
		utf8_normalize_nfc(request_var('race', '', true)), 
		utf8_normalize_nfc(request_var('gender', '', true)),
		utf8_normalize_nfc(request_var('class', '', true)), 
		utf8_normalize_nfc(request_var('age', '', true)), 
		utf8_normalize_nfc(request_var('dragonsworn', '', true)),
		utf8_normalize_nfc(request_var('dfalignment', '', true)),
		utf8_normalize_nfc(request_var('physique', '', true)),
		utf8_normalize_nfc(request_var('magicchoice', '', true)),
		utf8_normalize_nfc(request_var('languages', '', true)),
		utf8_normalize_nfc(request_var('characterpersonality', '', true)),
		utf8_normalize_nfc(request_var('irlpersonality', '', true)),
		utf8_normalize_nfc(request_var('interests', '', true)),
		utf8_normalize_nfc(request_var('minorspells', '', true)),
		utf8_normalize_nfc(request_var('prestige', '', true)),
		utf8_normalize_nfc(request_var('backstory', '', true)),
		utf8_normalize_nfc(request_var('dragonknowic', '', true)),
		utf8_normalize_nfc(request_var('dragonknowooc', '', true)),
		utf8_normalize_nfc(request_var('ic-reason', '', true)),
		utf8_normalize_nfc(request_var('ooc-reason', '', true)),
		utf8_normalize_nfc(request_var('misc-rp', '', true)),
		utf8_normalize_nfc(request_var('understand', '', true)),
		utf8_normalize_nfc(request_var('timeline', '', true)),
		utf8_normalize_nfc(request_var('lore', '', true))


		); 
		// variables to hold the parameters for submit_post
		$poll = $uid = $bitfield = $options = ''; 

		generate_text_for_storage($apply_post, $uid, $bitfield, $options, true, true, true);

		$data = array( 
			'forum_id'      => 8,
			'icon_id'		=> 0,

			'enable_bbcode'		=> true,
			'enable_smilies'	=> true,
			'enable_urls'		=> true,
			'enable_sig'		=> true,

			'message'		=> $apply_post,
			'message_md5'	=> md5($apply_post),
						
			'bbcode_bitfield'	=> $bitfield,
			'bbcode_uid'		=> $uid,

			'post_edit_locked'	=> 0,
			'topic_title'		=> $apply_subject,
			'notify_set'		=> false,
			'notify'			=> false,
			'post_time' 		=> 0,
			'forum_name'		=> '',
			'enable_indexing'	=> true,
		);

		// Sending the post to the forum set in configuration above
		submit_post('post', $apply_subject, '', POST_NORMAL, $poll, $data);

		$message = $user->lang['APPLICATION_SEND'];
		$message = $message . '<br /><br />' . sprintf($user->lang['RETURN_INDEX'], '<a href="' . append_sid("{$phpbb_root_path}index.$phpEx") . '">', '</a>');
		trigger_error($message);
}

page_header($user->lang['APPLICATION_PAGETITLE']);

$template->assign_vars(array(
	'PROCESS_APPFORM'	=> append_sid("{$phpbb_root_path}application_mortal.$phpEx"),
	));
	
$template->set_filenames(array(
    'body' => 'appform_body_mortal.php',
));

page_footer();


?>

